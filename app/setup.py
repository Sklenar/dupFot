import sys, platform, stat
import pip

#Soubor pro zkompilovani aplikace
#--------------------------------------------------------------------------------------------
def getTargetName():
	n='dupFot'
	
	myOS = platform.system()
	if myOS == 'Linux':
		return n
	elif myOS == 'Windows':
		return n+".exe"
	else:
		return n+".dmg"
#--------------------------------------------------------------------------------------------
def instImp(pack):
	try:
		imported = getattr(__import__('*', fromlist=[pack]), pack)
	except:
		pip.main(['install',pack])
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
try:
	from cx_Freeze import setup, Executable
except:
	pip.main(['install','cx_Freeze'])
	from cx_Freeze import setup, Executable

instImp('Pillow')
instImp('XlsxWriter')
instImp('piexif')
instImp('pyqt5')
instImp('send2trash')
instImp('numpy')
instImp('opencv-python')
instImp('scikit-image')
instImp('scipy')
instImp('python-dateutil')
#--------------------------------------------------------------------------------------------
name=getTargetName()
version='2.1'
url='https://gitlab.com/Sklenar/dupFot.git'
license='GNU'
author='xsklen09'
author_email='xsklen@stud.fit.vutbr.cz'
description='Tato bakalářská práce se zabývá analýzou, návrhem, implementací a otestováním aplikace, která slouží k vyhledání duplíkací ve fotografiích podle Exif metadat. Aplikace dále umožňuje zobrazení náhledu fotografie, včetně Exif metadat. Filtrování fotografií. Seskupení duplikací, následný výběr nejlepší fotografie pro ponechání podle parametru nastaveného uživatelem, manuální úpravu této volby a vymazání ostatních. Export vybraných fotografií do ZIP archivu.'

buildFolder='./build/'

main="main.py"
icon="./resources/dupFot.ico"

base = None
if sys.platform == "win32":
	base = "Win32GUI"

exe = Executable(script = main, base=base, targetName = getTargetName(),icon=icon)

build_options = {
	"packages": ['os','PIL','piexif','pyqt5','send2trash','pickle','numpy','cv2','skimage','scipy','dateutil'],
	"excludes": ['tkinter','scipy.spatial.cKDTree'],
	"include_files":['./resources/'],
	'build_exe': buildFolder+platform.system()
}
#--------------------------------------------------------------------------------------------
setup(
	name=name,
	version=version,
	url=url,
	license=license,
	author=author,
	author_email=author_email,
	description=description,
	options = {"build_exe": build_options},
	executables = [exe]
)

if name=='dupFot':
	st = os.stat(buildFolder+platform.system()+name)
	os.chmod(buildFolder+platform.system()+"/"+name, st.st_mode|0o111)