from threading import Thread

#Soubor pro zakladni tridy, ktere se netykaji GUI
#(c)xsklen09 (Artonix14), FIT VUT Brno, 2018
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#Trida pro nastaveni aplikace
class Settings:
	def __init__(self,toRecBin,pM,sF,SoI,pMn,cD,oD,tG,pMoo,cOf,qM,qMp,st,nOg,fs,sG,cWe,stp):
		self.toRecBin=toRecBin
		self.preferenceMeasure=pM
		self.subFolders = sF
		self.sizeOfImage=SoI
		self.preferenceMeasureNeg=pMn
		self.completeDupl=cD
		self.onlyDupl = oD
		self.timeGap=tG
		self.preferenceMeasureOnlyOne=pMoo
		self.compOnlyFileName=cOf
		self.qMeasure=qM
		self.qMeasureParametres=qMp
		self.stats=st
		self.numOfGroups=nOg
		self.fullScreen=fs
		self.sortGrups=sG
		self.compWithExif=cWe
		self.step=stp
#--------------------------------------------------------------------------------------------
#Trida pro nacteni zdroju
class Loading:
	def __init__(self):
		self.done=1
		self.max=1
		self.retComPic=[]
		self.startTime=0
		self.deleted=0
		self.prevTime=0
		self.prevTimeCalc=0.00
		self.sigLoad=None
		self.sorted=True
		self.sortedKey=""
		self.qM=[]
		self.setText=None
		self.editMet = 0
		self.pics=[]
		self.picsZ=[]
		self.k=None
		self.stop=False
#--------------------------------------------------------------------------------------------
#Trida pro uchovani vsech oken aplikace
class Windows:
	def __init__(self):
		self.list=[]
#--------------------------------------------------------------------------------------------
#Trida pro uchovani vsech jader aplikace
class Threads:
	def __init__(self):
		self.list=[]
#--------------------------------------------------------------------------------------------
#Trida pro uchovani casoveho rozmezi, pro moznost fitrace
class DateInterval:
	def __init__(self,start,end,include):
		self.start=start
		self.end=end
		self.include=include
	def __str__(self):
		if(self.include):
			return "Od "+self.start.toString('dd.MM.yyyy')+" do "+self.end.toString('dd.MM.yyyy')+", zahrnout."
		else:
			return "Od "+self.start.toString('dd.MM.yyyy')+" do "+self.end.toString('dd.MM.yyyy')+ ", nezahrnout."
#--------------------------------------------------------------------------------------------
#Trida pro uchovani filtru aplikace, zatim pouze casove useky
class Filters:
	def __init__(self):
		self.dateList=[]
#--------------------------------------------------------------------------------------------
class StoppableThread(Thread):
	def __init__(self,  name, target, args=()):
		super(StoppableThread, self).__init__(name=name, target=target, args=args)
		self._status = 'running'
		self.res=[]

	def stop(self):
		if (self._status == 'running'):
			self._status = 'stopped'
	def pick(self):
		if (self._status == 'stopped'):
			self._status = 'picked'
			return self.res
		else:
			return []

	def isRunning(self):
		return (self._status == 'running')
	def isStopped(self):
		return (self._status == 'stopped' or self._status == 'picked')
	def isPicked(self):
		return (self._status == 'picked')