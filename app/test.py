import statistics
import xlsxwriter

from gui import *

#Soubor pro testovani aplikace
#(c)xsklen09 (Artonix14), FIT VUT Brno, 2018
#--------------------------------------------------------------------------------------------
#Funkce pro vyhledani prumerne hodnoty v poli cisel
def average(vals):
	return statistics.median(vals)
#--------------------------------------------------------------------------------------------
#Funkce pro provedeni testovani, 10 vzorku pro kazdou operaci
def testing(worksheet,prep):
	global row,col,app,testFolder
	
	b=BaseBox()
	
	folders=[]
	
	count=10
	
	files=0
	ignFiles=0
	duplsGroups=0
	dupls=0
	
	resTimes=[]
	
	for c in range(1,42):
		for i in range(0,count):
			loading.retComPic=[]
			loading.done=0
			
			startTime=time.strftime('%d.%m.%Y, %H:%M:%S')
			
			print(str(c)+'.'+str(i)+' - '+startTime)
			
			startTime=time.time()
			
			b.findDupls()
			
			while(not(len(b.folders)==0) and (((len(loading.retComPic))==0) or ((int((loading.done/loading.max)*100))<100))):
				continue
			
			endTime=time.time()
			
			resTime=endTime-startTime
			resTimes.append(resTime)
		
		averageResTime=average(resTimes)
		#resTimeF=stopWatch(averageResTime)
		
		if not(len(loading.retComPic)==0):
			for d in loading.retComPic[0][0]:
				if(len(d[1])==1):
					files+=1
					continue
				
				duplsGroups+=1
				
				for f in d[1]:
					dupls+=1
					files+=1
			for i in loading.retComPic[0][1]:
				ignFiles+=1
				
		
		dupls-=duplsGroups
		#min=averageResTime/60
		secs=averageResTime
		
		worksheet.write(row, col,files)
		col+=1
		worksheet.write(row, col,ignFiles)
		col+=1
		worksheet.write(row, col,duplsGroups)
		col+=1
		worksheet.write(row, col,dupls)
		col+=1
		worksheet.write(row, col,count)
		col+=1
		worksheet.write(row, col,secs)
		col+=1
		
		"""
		print(files)
		print(dupls)
		print(duplsGroups)
		"""
		
		files=0
		ignFiles=0
		duplsGroups=0
		dupls=0
		resTimes=[]
		
		#pridani slozky
		b=None
		app.exit()
		
		b=BaseBox()
		folders.append([testFolder+str(c),prep])
		b.setFolders(folders)
		
		row+=1
		col=0
		
	
#--------------------------------------------------------------------------------------------
#Funkce pro zalozeni noveho excel souboru pro zapis vysledku
def newWorkSheet(filename):
	global row,col
	
	row = 0
	col = 0
	
	
	workbook = xlsxwriter.Workbook(filename)
	worksheet = workbook.add_worksheet()
	
	head=['pictures','ignoredPictures','duplicationsGroups','duplications','number of cycles','secs']
	
	for he in head:
		worksheet.write(row, col,he)
		col+=1
		
	
	data_format = workbook.add_format({'bg_color': '#00C7CE'})
	
	worksheet.set_row(row, cell_format=data_format)
	
	row += 1
	col=0
	
	return [workbook,worksheet]
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
row = 0
col = 0

testFolder="D:/test/"

"""
wb1,ws1=newWorkSheet("testResults1.xlsx")

testing(ws1, False)
wb1.close()
"""

wb2,ws2=newWorkSheet("testResults2.xlsx")

testing(ws2, True)
wb2.close()
