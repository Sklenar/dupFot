import glob
import os

import datetime
from dateutil.relativedelta import relativedelta

import math

from PIL import Image,IptcImagePlugin
from PIL.ExifTags import TAGS

from send2trash import send2trash

import piexif

import multiprocessing
import threading

import numpy as np
import cv2
from skimage.restoration import estimate_sigma

from globalVars import *

#Soubor pro zakladni funkce aplikace
#(c)xsklen09 (Artonix14), FIT VUT Brno, 2018
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#Funkce pro urceni miry sumu ve fotografii
def detectNoise(im,treshold):
	image = cv2.imread(im.filename)
	n=estimate_sigma(image, multichannel=True, average_sigmas=True)
	n*=1000
	
	if(n<treshold):
		return [False, n]
	else:
		return [True, n]
#--------------------------------------------------------------------------------------------
#Funkce pro urceni miry rozmazani fotografie
def detetectBluryOfImage(im,treshold):
	image = cv2.imread(im.filename)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	fm = cv2.Laplacian(gray, cv2.CV_64F).var()
	
	if(fm<treshold):
		return [False,fm]
	else:
		return [True,fm]
#--------------------------------------------------------------------------------------------
#Funkce pro urceni miry barevnosti fotografie
def colorFullnessOfImage(im,treshold):
	image = cv2.imread(im.filename)
	
	(B, G, R) = cv2.split(image.astype("float"))
	
	rg = np.absolute(R - G)
	yb = np.absolute(0.5 * (R + G) - B)
	
	(rbMean, rbStd) = (np.mean(rg), np.std(rg))
	(ybMean, ybStd) = (np.mean(yb), np.std(yb))
	
	stdRoot = np.sqrt((rbStd ** 2) + (ybStd ** 2))
	meanRoot = np.sqrt((rbMean ** 2) + (ybMean ** 2))
	
	colorFullness=(stdRoot + (0.3 * meanRoot))*10
	
	if(colorFullness < treshold):
		return [False, colorFullness]
	else:
		return [True, colorFullness]
#--------------------------------------------------------------------------------------------
def qualityMeasure(pictures):
	ret=[]
	percentil=0
	blur=[]
	noise=[]
	colorFullness=[]
	
	sum=0
	
	q=[]

	for i,p in enumerate(pictures):
		try:
			predict=(getMetaData(p)['DateTimeOriginal']==getMetaData(pictures[i-1])['DateTimeOriginal'])
		except:
			predict=False
		
		if(i and predict):
			blur.append(blur[i-1])
			noise.append(noise[i-1])
			colorFullness.append(colorFullness[i-1])
		else:
			blur.append(detetectBluryOfImage(p,95))
			noise.append(detectNoise(p,95))
			colorFullness.append(colorFullnessOfImage(p,80))
	
	for i,p in enumerate(pictures):
		sum=0
		
		if(sett.qMeasureParametres[0]):
			if(blur[i][0]):
				sum += blur[i][1]
			else:
				sum += blur[i][1]/2
		if(sett.qMeasureParametres[1]):
			if(noise[i][0]):
				sum += noise[i][1]
			else:
				sum += noise[i][1]/ 2
		if(sett.qMeasureParametres[2]):
			if(colorFullness[i][0]):
				sum += colorFullness[i][1]
			else:
				sum += colorFullness[i][1]/ 2

		q.append(sum)
	
	m = max(q)
	
	if not(m):
		m=1
	
	for s in q:
		percentil=int((s/m)*100)
	
		ret.append(percentil)
	
	return ret
#--------------------------------------------------------------------------------------------
def calcQualityMeasure(pictures):
	ret=[]
	
	for g in pictures:
		if(loading.stop):
			break
		else:
			ret.append(qualityMeasure(g[1]))
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro urceni zda je retezec str cislo
def isNumber(str):
	try:
		val = int(str)
		return True
	except ValueError:
		return False
#--------------------------------------------------------------------------------------------
#Funkce pro urceni indexu prvku v listu
def getIndex(list,ind,element):
	index=None
	ind+=1
	
	for i,item in enumerate(list[ind:]):
		if((item.filename)==(element.filename)):
			index=i+ind
			break
	
	return index
#--------------------------------------------------------------------------------------------
#Funkce pro ziskani metadat fotografie, primarne ExIf potom IPTC
def getMetaData(file):
	ret = {}
	prep=True
	
	try:
		info = file._getexif()
	except:
		info=IptcImagePlugin.getiptcinfo(file)
		prep=False
	
	
	if(info==None):
		return None
	
	if(prep):
		for tag, value in info.items():
			decoded = TAGS.get(tag, tag)
			ret[decoded] = str(value).encode().decode('unicode-escape').replace("Copyright (C)","©").replace("Copyright ÃÂ","")
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro filtrovani zbytecnych metadat pri zobrazeni info u fotografie v GUI
def getInfExifData(file):
	exIfData=getMetaData(file)
	ret={}
	
	keys=["FileSource","UserComment","SceneType","DateTimeDigitized",
		  "DateTimeOriginal","FlashPixVersion","ExifVersion",
		  "ExifOffset","CFAPattern","CustomRendered","ComponentsConfiguration"]
	
	for k,v in exIfData.items():
		if((not(isNumber(k))) and (k not in keys)):
			ret[k]=v
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro ziskani nazvua slozky fotografie
def getFileName(pic):
	fnR=pic.filename.replace("/","\\")
	
	fn=fnR.split("\\")
	ret="/"+fn[len(fn)-2].replace("../","")+"/"+fn[len(fn)-1]
	
	return ret
# --------------------------------------------------------------------------------------------
# Funkce pro ziskani nazvu fotografie
def getName(pic):
	fnR = pic.filename.replace("/", "\\")

	fn = fnR.split("\\")
	ret = fn[len(fn) - 1]

	return ret
# --------------------------------------------------------------------------------------------
# Funkce pro prevedeni sekund do ruznych casovych jednotek
def stopWatch(val):
	d = val/86400
	days = int(d)

	h = (d - days) * 24
	hours = int(h)

	m = (h - hours) * 60
	minutes = int(m)

	s = (m - minutes) * 60
	seconds = int(s)

	ms = (s - seconds) * 1000
	milliseconds = int(ms)

	return [days, hours, minutes, seconds, milliseconds]
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani metadat dvou fotografii, vrati True pokud jsou stejne
def compPhotosExifs(file1,file2):
	if (sett.compOnlyFileName):
		if(file1.filename == file2.filename):
			return [True,""]

	exifData1=getMetaData(file1)
	exifData2=getMetaData(file2)

	return (compDictsValues(exifData1,exifData2))
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani dvou slovniku, vrati True pokud jsou stejne
def compDicts(dict1,dict2):
	if((len(dict1.items()))!=(len(dict2.items()))):
		#print(getUniqueKeys(dict1,dict2))
		return False
	
	for k,v in dict1.items():
		try:
			if(dict2[k]!=v):
				return False
		except KeyError:
			return False
	
	return True
#--------------------------------------------------------------------------------------------
#Funkce pro odstraneni zaznamu ze slovniků podle klice zaznamu
def removeKeys(dict1, dict2, keys):
	try:
		for k in keys:
			del dict1[k]
			del dict2[k]
	finally:
		return dict1,dict2
#--------------------------------------------------------------------------------------------
#Porovnani dvou casu s mezerou danou uzivatelem
def compDate(date1,date2):
	gap=sett.timeGap

	if(date1==date2):
		return True

	date1 = datetime.datetime.strptime(date1, "%Y:%m:%d %H:%M:%S")
	date2 = datetime.datetime.strptime(date2, "%Y:%m:%d %H:%M:%S")

	if(date1>=date2):
		diff=date1-date2
	else:
		diff =date2 - date1

	if(diff.total_seconds() <= gap):
		return True

	return False
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani hodnot ze zadanych slovniků, nektere zanzmay lze vynechat na zaklade nastaveni, vrati True pokud se shoduji
def compDictsValues(dict1,dict2):
	keys = ['Make', 'Model']

	try:
		if not(compDate(dict1['DateTimeOriginal'],dict2['DateTimeOriginal'])):
			return [False,'DateTimeOriginal']
	except KeyError:
		pass

	removeKeys(dict1, dict2, ["DateTimeOriginal", "DateTimeDigitized"])

	if not(sett.completeDupl):
		for k in keys:
			try:
				if (dict1[k] != dict2[k]):
					return [False,k]
			except KeyError:
				pass

		return [True,""]

	elif (not (sett.sizeOfImage)):
		dict1, dict2 = removeKeys(dict1, dict2, ["ImageWidth", "ImageLength","ExifImageWidth","ExifImageHeight"])

	for k,v in dict1.items():
		try:
			if(dict2[k]!=v):
				return [False,k]
		except KeyError:
			pass

	return [True,""]
#--------------------------------------------------------------------------------------------
#Funkce pro ziskani nazvu zanzmu ve kterych se zadane slovniky lisi
def getUniqueKeys(dict1,dict2):
	ret=[]
	
	if((len(dict1.items()))<(len(dict2.items()))):
		dict1,dict2=dict2,dict1
	
	for k in dict1.keys():
		try:
			p=dict2[k]
		except KeyError:
			ret.append(k)
	
	return ret
#--------------------------------------------------------------------------------------------
def indexOfValueInList(fn, array):
	for i,s in enumerate(array):
		if (s.filename == fn):
			return i

	return -1
#--------------------------------------------------------------------------------------------
def indexOfValueInListStr(fn, array):
	for i,s in enumerate(array):
		if (s == fn):
			return i

	return -1
#--------------------------------------------------------------------------------------------
#Funkce pro zjisteni zda je nazev fn jiz v listu seen, vrati True pokud ano
def valueInList(fn,seen):
	for s in seen:
		if(s==fn):
			return True
	
	return False
#--------------------------------------------------------------------------------------------
#Funkce pro predrazeni fotografii
def sortPictures(array):
	ret=array
	keys1=['DateTimeOriginal','DateTimeDigitized']
	keys2=['Make', 'Model']

	for k in keys1:
		try:
			ret.sort(key = lambda x: datetime.datetime.strptime(getMetaData(x)[k], "%Y:%m:%d %H:%M:%S"))
			loading.sortedKey=k
			return ret
		except:
			pass

	for k in keys2:
		try:
			ret.sort(key = lambda x: (getMetaData(x)[k]).lower())
			loading.sortedKey = k
			return ret
		except:
			pass

	loading.sorted=False
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro ziskani vsech fotografii ze zadanych slozek, vraci list ignorovanych fotek bez exIf a list fotek s metadaty pro vyhledani duplikaci
def getPicturesFromFolders(folders):
	ret=[]
	ignored=[]
	seen=[]

	threads=Threads()
	
	for f,s in folders:
		f=fixPath(f)
		
		t = Thread(target=searchFolder, args=(f,s,ret,ignored,seen,))
		threads.list.append(t)
		t.start()

	while not(len(threads.list)==0):
		joinThread(0, threads)

	ret=sortPictures(ret)

	return [ignored,ret]
#--------------------------------------------------------------------------------------------
#Funkce pro prohledani zadane slozky
def searchFolder(f,s,ret,ignored,seen):
	allFolders=[]
	
	if (s):
		for subFolders in os.walk(f):
			for subFolder in subFolders:
				if (isinstance(subFolder, str)):
					allFolders.append(subFolder)
	else:
		allFolders.append(f)
	
	for folderName in allFolders:
		for fileName in (glob.glob(folderName + "/*.*")):
			if('.psd' not in fileName):
				try:
					im = Image.open(fileName)
				except:
					continue
			else:
				continue

			if (not (valueInList(im.filename, seen))):
				seen.append(getFileName(im))
			else:
				continue

			if (getMetaData(im) != None):
				ret.append(im)
			else:
				ignored.append(im)
#--------------------------------------------------------------------------------------------
def fixPath(path):
	ret=path
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro vymazani zvolenych fotografii z listu duplikaci
def delDups(dupls,toRecycleBin):
	count=0
	d=0
	ret=[]
	pom=[]

	for dupl in dupls:
		pom.append(dupl[0].copy())
		pom.append(dupl[1].copy())

		ret.append(pom)
		pom=[]


	for indD,dupl in enumerate(dupls):
		for ind,file in enumerate(dupl[1]):
			if(not(ind in dupl[0])):
				ret[indD][1].remove(file)

				file.close()
			
				if(toRecycleBin):
					f=str(file.filename.replace("/", "\\"))

					send2trash(f)
				else:
					os.remove(file.filename)

				count+=1

	return count,ret
#--------------------------------------------------------------------------------------------
#Funkce pro ulozeni zmenenych exIf metadat
def changeSaveExIfMet(file,metName,metValue):
	exif_dict = piexif.load(file.info["exif"])
	
	exif_dict["Exif"][getattr(piexif.ExifIFD,metName)] = metValue
	exif_bytes = piexif.dump(exif_dict)
	
	file.save(file.filename,quality=100,exif=exif_bytes)
#--------------------------------------------------------------------------------------------
def xstr(s):
	if s is None:
		return 'NOTHING'
	
	return str(s)
#--------------------------------------------------------------------------------------------
#Funkce pro ulozeni zmenenych exIf GPS metadat
def changeSaveGPSMet(file,metName,metValue):
	exif_dict = piexif.load(file.info["exif"])
	
	#("0th", "Exif", "GPS", "1st"):
	exif_dict["GPS"][getattr(piexif.GPSIFD,metName)] = metValue
	exif_bytes = piexif.dump(exif_dict)
	
	file.save(file.filename,quality=100,exif=exif_bytes)
#--------------------------------------------------------------------------------------------
#Funkce pro automaticky vyber ve skupinach duplikaci
def solvDup(duplicities,prepQ):
	if(sett.preferenceMeasureOnlyOne):
		if (prepQ == 0):
			return [][:1]
		elif (prepQ == 1):
			return [index for index, value in enumerate(duplicities)][:1]
		elif (prepQ == 2):
			return compSizeOfFiles(duplicities, sett.preferenceMeasureNeg)[:1]
		elif (prepQ == 3):
			return compDateOfChangeOfFiles(duplicities, sett.preferenceMeasureNeg)[:1]
		elif (prepQ == 4):
			return compDateOfMaking(duplicities, sett.preferenceMeasureNeg)[:1]
		elif (prepQ == 5):
			return compFolder(duplicities)[:1]
		elif (prepQ == 6):
			return compResolution(duplicities, sett.preferenceMeasureNeg)[:1]
		elif (prepQ == 7):
			return compQuality(duplicities, sett.preferenceMeasureNeg)[:1]
	else:
		if (prepQ == 0):
			return []
		elif (prepQ == 1):
			return [index for index, value in enumerate(duplicities)]
		elif (prepQ == 2):
			return compSizeOfFiles(duplicities, sett.preferenceMeasureNeg)
		elif (prepQ == 3):
			return compDateOfChangeOfFiles(duplicities, sett.preferenceMeasureNeg)
		elif (prepQ == 4):
			return compDateOfMaking(duplicities, sett.preferenceMeasureNeg)
		elif (prepQ == 5):
			return compFolder(duplicities)
		elif (prepQ == 6):
			return compResolution(duplicities, sett.preferenceMeasureNeg)
		elif (prepQ == 7):
			return compQuality(duplicities, sett.preferenceMeasureNeg)
#--------------------------------------------------------------------------------------------
#Funkce pro zjisteni slozky ktera obsahuje zadanou fotografii
def getFolder(im):
	return getFileName(im).split("/")[1]
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani slozek ktere obsahuji zadane duplicity
def compFolder(duplicities):
	ret=[0]
	folder=getFolder(duplicities[0])
	
	for i,d in enumerate(duplicities):
		if(getFolder(d)==folder):
			ret.append(i)
	
	return ret
#--------------------------------------------------------------------------------------
#Prevod velikosti
def convertSize(size_bytes):
	if size_bytes == 0:
		return "0B"

	size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
	i = int(math.floor(math.log(size_bytes, 1024)))
	p = math.pow(1024, i)
	s = round(size_bytes / p, 2)

	return "%s %s" % (s, size_name[i])
#--------------------------------------------------------------------------------------
#Funkce pro porovnani velikosti fotografii
def compSizeOfFiles(pictures,prep):
	res=[]
	ret=[]
	
	for p in pictures:
		res.append(os.path.getsize(p.filename))
	
	m = max(res)
	indexes=[i for i, j in enumerate(res) if j == m]
	
	if(prep):
		indexes=reversed(indexes)
	
	ret.extend(indexes)
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani kvality fotografii
def compQuality(pictures,prep):
	sett.qMeasure=True

	ret=[]
	quality=qualityMeasure(pictures)

	loading.qM.append(quality)

	m = max(quality)
	indexes=[i for i, j in enumerate(quality) if j == m]

	if(prep):
		indexes=reversed(indexes)

	ret.extend(indexes)

	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani rozliseni fotografii
def compResolution(pictures,prep):
	res=[]
	ret=[]

	for p in pictures:
		width, height = p.size
		res.append(width*height)

	m = max(res)
	indexes=[i for i, j in enumerate(res) if j == m]

	if(prep):
		indexes=reversed(indexes)

	ret.extend(indexes)

	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro porovnani podle datumu posledni modifikace fotografie
def compDateOfChangeOfFiles(pictures, prep):
	res=[]
	ret=[]
	
	for p in pictures:
		res.append(os.path.getmtime(p.filename))
	
	m = max(res)
	indexes=[i for i, j in enumerate(res) if j == m]
	
	if(prep):
		indexes=reversed(indexes)
	
	ret.extend(indexes)
	
	return ret
#--------------------------------------------------------------------------------------------
def compDateOfMaking(pictures, prep):
	res = []
	ret = []

	for p in pictures:
		try:
			res.append(time.strptime(getMetaData(p)['DateTimeOriginal'], "%Y:%m:%d %H:%M:%S"))
		except KeyError:
			return []

	m = max(res)
	indexes = [i for i, j in enumerate(res) if j == m]

	if (prep):
		indexes = reversed(indexes)

	ret.extend(indexes)

	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro ziskani fotografii v zadanem intervalu z vyberu
def getPicturesFromTo(pictures,rangeFrom,rangeTo):
	ret=[]
	res=[]
	formatDate = "%d.%m.%Y %H:%M:%S"
	
	#rangeFrom=datetime.strptime(rangeFrom, formatDate)
	#rangeTo=datetime.strptime(rangeTo, formatDate)
	
	for p in pictures:
		res.append([p, getMetaData(p)["DateTimeOriginal"]])
	
	for d in res:
		date=datetime.datetime.strptime(d[1], "%Y:%m:%d %H:%M:%S")
	
		if(rangeFrom<=date<=rangeTo):
			ret.append(d[0])
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro vymazani fotografii v zadanem intervalu z vyberu
def delPicturesFromTo(pictures,rangeFrom,rangeTo):
	ret=[]
	res=[]
	formatDate = "%d.%m.%Y %H:%M:%S"

	#rangeFrom=datetime.strptime(rangeFrom, formatDate)
	#rangeTo=datetime.strptime(rangeTo, formatDate)
	
	for p in pictures:
		res.append([p, getMetaData(p)["DateTimeOriginal"]])
	
	for d in res:
		date=datetime.datetime.strptime(d[1], "%Y:%m:%d %H:%M:%S")
	
		if(not(rangeFrom<=date<=rangeTo)):
			ret.append(d[0])
	
	return ret
#--------------------------------------------------------------------------------------------
#Funkce pro filtrovani fotografii podle zadanych casovych intervalu
def filterByDate(pictures,datesRanges):
	ret=[]
	dRALL=True

	if(not(len(datesRanges))):
		return pictures

	for dR in datesRanges:
		if(dR.include):
			ret.extend(getPicturesFromTo(pictures,dR.start,dR.end))
			datesRanges.remove(dR)
			dRALL = False
	if (dRALL):
		ret = pictures

	for dR in datesRanges:
		ret=delPicturesFromTo(ret,dR.start,dR.end)
	
	return ret
#--------------------------------------------------------------------------------------------
# Funkce pro zmenu datumu v exIf metadatech
def chngDat(im, prep, y,m,d,h,mm,s):
	info=getMetaData(im)["DateTimeOriginal"]

	date=datetime.datetime.strptime(info,"%Y:%m:%d %H:%M:%S")

	if (prep):
		date = date + relativedelta(years=+y, months=+m, days=+d, hours=+h, minutes=+mm, seconds=+s)
	else:
		date = date - relativedelta(years=+y, months=+m, days=+d, hours=+h, minutes=+mm, seconds=+s)

	changeSaveExIfMet(im,"DateTimeOriginal",datetime.datetime.strftime(date,"%Y:%m:%d %H:%M:%S"))
	changeSaveExIfMet(im, "DateTimeDigitized", datetime.datetime.strftime(date, "%Y:%m:%d %H:%M:%S"))
#--------------------------------------------------------------------------------------------
#Funkce pro zmenu datumu v exIf metadatech
def changeDate(date,prep,value):
	dateP=value.replace(" ",":")
	dateP = dateP.replace(".", ":")
	dateP = dateP.replace("/", ":")
	
	dateP=dateP.split(":")
	
	y=int(dateP[0])
	m=int(dateP[1])
	d=int(dateP[2])
	h=int(dateP[3])
	mm=int(dateP[4])
	s=int(dateP[5])
	
	if(prep):
		date=date+relativedelta(years=+y,months=+m,days=+d,hours=+h,minutes=+mm,seconds=+s)
	else:
		date=date-relativedelta(years=+y,months=+m,days=+d,hours=+h,minutes=+mm,seconds=+s)
	
	return date
#--------------------------------------------------------------------------------------------
def dupl(source, sourceFN, index, item, threadsCom):
	lockToDelete = threading.Lock()

	if (len(source)):
		loading.max = len(source) - 1
	else:
		loading.max = 0

	threadsCom.list[index].res.append(item)

	for i, p in enumerate(source[index + 1:]):
		if (threadsCom.list[index].isStopped() or loading.stop):
			break

		# print(getFileName(item)+' | '+getFileName(p))
		cmpPhEx=compPhotosExifs(item, p)

		if(cmpPhEx[0]):
			indOfValInList = indexOfValueInListStr(p.filename, sourceFN)

			if not (threadsCom.list[indOfValInList].isStopped()):
				threadsCom.list[index].res.append(p)
			else:
				threadsCom.list[index].res += threadsCom.list[indOfValInList].pick()
				"""
				with lockToDelete:
					del source[indexOfValueInList(p.filename, source[index + 1:])]
				"""
			threadsCom.list[indOfValInList].stop()
		elif(loading.sorted and loading.sortedKey==cmpPhEx[1]):
			#nasel jsem odlisnou v serazenem poli, vo hodnote podle ktere bylo serazeno, mam jistotu ze dalsi uz se budou lisit minimalne v teto hodnote vzdy, tedy muzu skoncit
			break

	loading.done += 1
	loading.sigLoad.emit()
#--------------------------------------------------------------------------------------------
def runProcess(queue):
	for t in iter(queue.get, None):
		t.start()

	queue.join()
#--------------------------------------------------------------------------------------------
def runThreadsOnMultipleCores(threads):
	numOfCores=multiprocessing.cpu_count()
	processes=[]
	q=multiprocessing.Queue()

	for i in range(1,numOfCores+1):
		for t in threads.list[numOfCores*(i-1):numOfCores*i]:
			q.put(t)

		processes.append(multiprocessing.Process(target=runProcess,args=(q,)))

		q = multiprocessing.Queue()

	for p in processes:
		p.start()

	for p in processes:
		p.join()
#--------------------------------------------------------------------------------------------
#Funkce pro spojeni vlakna s hlavnim vlaknem, dulezita pro synchronizaci vlaken
def joinThread(index,threads):
	threads.list[index].join()
	threads.list.pop(index)
#--------------------------------------------------------------------------------------------