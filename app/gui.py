from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from os.path import basename
import zipfile
import time


from lib import *

#Soubor pro tridy a funkce pro GUI
#(c)xsklen09 (Artonix14), FIT VUT Brno, 2018
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
f=QFont("Arial", 10)
f2=QFont("Arial", 12)

f3 = QFont("Arial", 18)
f3.setBold(True)
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeni seznamu ignorovanych fotografii, ignorovane oftografie je take mozne smazat
class IgnoredPicturesBox(QWidget):
	#Metoda pro inicializaci komponenty
	def __init__(self,ignored):
		super().__init__()
		self.title = 'Ignorované fotografie'
		self.setWindowIcon(self.style().standardIcon(QStyle.SP_MessageBoxInformation))
		self.scrollArea=QScrollArea(self)
		self.height=275
		self.width=577
		self.ignored=ignored
		
		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		inf=""
		line=1
		maxFn=23
		self.setWindowTitle(self.title)
		
		for pic in self.ignored:
			fn=getFileName(pic)
			inf+=fn+"\n"
			line+=1
			if(len(fn)>maxFn):
				maxFn=len(fn)

		label = QLabel(inf, self)
		label.setFont(f2)
		label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		label.move(1,0)
		label.width=130+10.5*maxFn
		label.height = 30*line+30
		
		self.setFixedSize(self.width, self.height)
		
		scroLayoutV = QVBoxLayout()
		scroLayoutV.addWidget(label)
		widget = QWidget()
		widget.setLayout(scroLayoutV)
		self.scrollArea.setWidget(widget)
		
		self.scrollArea.setFixedSize(self.width-2, self.height-40)
		self.scrollArea.move(1,1)
		self.scrollArea.setWidgetResizable(True)
		
		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())
		
		btn = QPushButton("Zavřít", self)
		btn.setFont(f)
		btn.clicked.connect(lambda:self.close)
		btn.clicked.connect(self.close)
		btn.move(self.width/2+100,self.height/2+105)

		btnAccept = QPushButton("Smazat", self)
		btnAccept.setFont(f)
		btnAccept.clicked.connect(lambda: showYesNoDupBox(self, self.ignored,1))
		btnAccept.move(self.width / 2 -200,self.height/2+105)
		
		self.show()
		#--------------------------------------------------------------------------------------------
		# Metoda ktera se vykona automaticky pri ukonceni komponenty
		def closeEvent(self, event):
			windows.list.remove(self)
			
			event.accept()
#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeni oznameni, uspesne smazani fotografii, chyba ulozeni exportu,...
class NotificationBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self,text,tit,prep):
		super().__init__()
		self.title = tit
		self.setWindowIcon(self.style().standardIcon(QStyle.SP_MessageBoxInformation))
		self.text=text
		self.prep=prep
		
		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)
		lh=QHBoxLayout(self)
		lh.setContentsMargins(0,0,0,0)
		lv=QVBoxLayout(self)
		lv.setContentsMargins(2, 0, 2, 1)

		label = QLabel(self.text, self)
		label.setFont(f2)
		label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		label.setAlignment(Qt.AlignCenter)

		self.setWindowFlags(Qt.WindowCloseButtonHint)
		
		btnAccept = QPushButton("Pokračovat", self)
		btnAccept.setFont(f)
		btnAccept.clicked.connect(self.close)

		lh.addStretch()
		lh.addWidget(btnAccept)
		lh.addStretch()

		lv.addWidget(label)

		tmp = QWidget()
		tmp.setLayout(lh)
		lv.addWidget(tmp)

		self.setLayout(lv)

		self.setFixedSize(label.sizeHint().width()+10,label.sizeHint().height()+40)
		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())

		self.show()
		#--------------------------------------------------------------------------------------------
		# Metoda ktera se vykona automaticky pri ukonceni komponenty
		def closeEvent(self, event):
			if not(self.prep):
				closeDuplWindows()
			
			event.accept()
#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeninacitani fotografii, zobrazuje prubeh vyhledani duplikaci v %
class LoadingBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self):
		super().__init__()
		self.title="Načítání fotografií"
		self.pBar = QProgressBar(self)
		loading.startTime=time.time()
		self.labTime=QLabel("",self)
		self.lab=QLabel("Zbývající čas:",self)
		self.prep=False
		self.prp=True

		loading.stop=False

		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		self.pBar.setFont(f2)
		pBarWidth=350
		pBarHeight = 30
		self.pBar.setGeometry(5,5,pBarWidth,pBarHeight)
		self.pBar.setValue(0)

		self.setFixedSize(pBarWidth+10,pBarHeight+40)

		self.lab.setFont(f2);
		self.lab.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		self.lab.width = 180
		self.lab.height = 72
		self.lab.setGeometry(5,15, self.lab.width, self.lab.height)

		self.labTime.setText("Počítám...")
		self.labTime.setFont(f2);
		self.labTime.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		self.labTime.width = 150
		self.labTime.height = 72
		self.labTime.setGeometry(140, 15, self.labTime.width, self.labTime.height)

		btnStop=QPushButton("",self)
		btnStop.setIcon(self.style().standardIcon(QStyle.SP_MediaStop))
		btnStop.clicked.connect(self.stop)
		btnStop.move(pBarWidth/2+140, pBarHeight/2+24)

		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())
		
		self.setWindowFlags(Qt.WindowTitleHint | Qt.CustomizeWindowHint)

		loading.setText=self.setValStr
		
		self.show()
	#--------------------------------------------------------------------------------------------
	def stop(self):
		loading.stop=True
		self.close()

		if (len(windows.list)):
			windows.list[0].disableAllowElements(True)
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		windows.list.remove(self)

		event.accept()
	#--------------------------------------------------------------------------------------------
	# Metoda pro prijmuti signalu na zmenu % v postupu
	def signalEmit(self):
		actTime=time.time()

		if(not(loading.max==0)):
			val=int((loading.done/loading.max)*100)
		else:
			val=100

		if(val==0):
			val=1

		if((val>1) and not(self.pBar.value()==val) and self.prp):
			self.updateTime(actTime, val)

		#print(loading.done,' | ',loading.max,' = ',val)

		self.pBar.setValue(val)
	#--------------------------------------------------------------------------------------------
	def setValStr(self,val):
		self.prp=False
		self.labTime.setText("")
		self.lab.setText(val)
	#--------------------------------------------------------------------------------------------
	def updateTime(self,actTime,percent):
		val=(((actTime-loading.startTime)/percent)*(100-percent))

		if((self. prep) or not(loading.prevTime==0) and (loading.prevTimeCalc<val)):
			val = (((actTime - loading.prevTime) / 2) * (100 - percent))
			self.prep=True

		loading.prevTimeCalc = val
		loading.prevTime = actTime

		val=stopWatch(val)

		self.labTime.setText(str(val[0])+"d "+str(val[1])+"h "+str(val[2])+"m "+str(val[3])+"s")
#--------------------------------------------------------------------------------------------
#
class QtImageViewer(QGraphicsView):
	"""
	Mouse interaction:
		Left mouse button drag: Pan image.
		Right mouse button drag: Zoom box.
		Right mouse button doubleclick: Zoom to show entire image.
		Wheel for zoom.
	"""

	leftMouseButtonPressed = pyqtSignal(float, float)
	leftMouseButtonReleased = pyqtSignal(float, float)
	leftMouseButtonDoubleClicked = pyqtSignal(float, float)

	rightMouseButtonReleased = pyqtSignal(float, float)
	rightMouseButtonPressed = pyqtSignal(float, float)
	rightMouseButtonDoubleClicked = pyqtSignal(float, float)

	def __init__(self,parent,im):
		super(QtImageViewer, self).__init__(parent)

		self.scene = QGraphicsScene()
		self.setScene(self.scene)

		self._pixmapHandle = None

		self.aspectRatioMode = Qt.KeepAspectRatio

		self.zoomStack = []

		self.canZoom = True
		self.canPan = True

		self._zoom = 0

		self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
		self.setBackgroundBrush(QBrush(QColor(30, 30, 30)))
		self.setFrameShape(QFrame.NoFrame)

		self.setImage(QPixmap(im.filename))
	#--------------------------------------------------------------------------------------------
	def hasImage(self):
		return self._pixmapHandle is not None
	#--------------------------------------------------------------------------------------------
	def clearImage(self):
		if self.hasImage():
			self.scene.removeItem(self._pixmapHandle)
			self._pixmapHandle = None
	#--------------------------------------------------------------------------------------------
	def pixmap(self):
		if self.hasImage():
			return self._pixmapHandle.pixmap()
		return None
	#--------------------------------------------------------------------------------------------
	def image(self):
		if self.hasImage():
			return self._pixmapHandle.pixmap().toImage()
		return None
	#--------------------------------------------------------------------------------------------
	def setImage(self, image):
		if type(image) is QPixmap:
			pixmap = image
		elif type(image) is QImage:
			pixmap = QPixmap.fromImage(image)
		else:
			raise RuntimeError("ImageViewer.setImage: Argument must be a QImage or QPixmap.")
		if self.hasImage():
			self._pixmapHandle.setPixmap(pixmap)
		else:
			self._pixmapHandle = self.scene.addPixmap(pixmap)
		self.setSceneRect(QRectF(pixmap.rect()))  # Set scene size to image size.
		self.updateViewer()
	#--------------------------------------------------------------------------------------------
	def loadImageFromFile(self, fileName=""):
		if len(fileName) == 0:
			if QT_VERSION_STR[0] == '4':
				fileName = QFileDialog.getOpenFileName(self, "Open image file.")
			elif QT_VERSION_STR[0] == '5':
				fileName, dummy = QFileDialog.getOpenFileName(self, "Open image file.")
		if len(fileName) and os.path.isfile(fileName):
			image = QImage(fileName)
			self.setImage(image)
	#--------------------------------------------------------------------------------------------
	def updateViewer(self):
		if not self.hasImage():
			return
		if len(self.zoomStack) and self.sceneRect().contains(self.zoomStack[-1]):
			self.fitInView(self.zoomStack[-1], Qt.IgnoreAspectRatio)  # Show zoomed rect (ignore aspect ratio).
		else:
			self.zoomStack = []  # Clear the zoom stack (in case we got here because of an invalid zoom).
			self.fitInView(self.sceneRect(), self.aspectRatioMode)  # Show entire image (use current aspect ratio mode).
	#--------------------------------------------------------------------------------------------
	def wheelEvent(self, event):
		if self.hasImage():
			if event.angleDelta().y() > 0:
				factor = 1.25
				self._zoom += 1
			else:
				factor = 0.8
				self._zoom -= 1

			if self._zoom > 0:
				self.scale(factor, factor)
			elif self._zoom == 0:
				self.updateViewer()
			else:
				self._zoom = 0

		self.callRest(event, 0)
	#--------------------------------------------------------------------------------------------
	def mousePressEvent(self, event):
		scenePos = self.mapToScene(event.pos())

		if event.button() == Qt.LeftButton:
			if self.canPan:
				self.setDragMode(QGraphicsView.ScrollHandDrag)
			self.leftMouseButtonPressed.emit(scenePos.x(), scenePos.y())
		elif event.button() == Qt.RightButton:
			if self.canZoom:
				self.setDragMode(QGraphicsView.RubberBandDrag)
			self.rightMouseButtonPressed.emit(scenePos.x(), scenePos.y())
		QGraphicsView.mousePressEvent(self, event)

		self.callRest(event, 1)
	#--------------------------------------------------------------------------------------------
	def mouseReleaseEvent(self, event,k=False):
		QGraphicsView.mouseReleaseEvent(self, event)

		scenePos = self.mapToScene(event.pos())
		if event.button() == Qt.LeftButton:
			self.setDragMode(QGraphicsView.NoDrag)
			self.leftMouseButtonReleased.emit(scenePos.x(), scenePos.y())
		elif event.button() == Qt.RightButton:
			if self.canZoom:
				viewBBox = self.zoomStack[-1] if len(self.zoomStack) else self.sceneRect()
				selectionBBox = self.scene.selectionArea().boundingRect().intersected(viewBBox)
				self.scene.setSelectionArea(QPainterPath())  # Clear current selection area.

				if(k):
					selectionBBox =k
				else:
					loading.k=selectionBBox

				if selectionBBox.isValid() and (selectionBBox != viewBBox):
					self.zoomStack.append(selectionBBox)
					self.updateViewer()
			self.setDragMode(QGraphicsView.NoDrag)
			self.rightMouseButtonReleased.emit(scenePos.x(), scenePos.y())

		self.callRest(event, 2)
	#--------------------------------------------------------------------------------------------
	def mouseDoubleClickEvent(self, event):
		scenePos = self.mapToScene(event.pos())
		if event.button() == Qt.LeftButton:
			self.leftMouseButtonDoubleClicked.emit(scenePos.x(), scenePos.y())
		elif event.button() == Qt.RightButton:
			if self.canZoom:
				self.zoomStack = []  # Clear zoom stack.
				self.zoom=0
				self.updateViewer()
			self.rightMouseButtonDoubleClicked.emit(scenePos.x(), scenePos.y())
		QGraphicsView.mouseDoubleClickEvent(self, event)

		self.callRest(event,3)
	#--------------------------------------------------------------------------------------------
	def resizeEvent(self, event):
		self.updateViewer()

		self.callRest(event, 4)
	#--------------------------------------------------------------------------------------------
	def callRest(self,event,prep):
		index=-1

		while(index==-1):
			try:
				index=loading.pics.index(self)
				del loading.pics[index]
			except ValueError:
				for p in loading.picsZ:
					loading.pics.append(p)


		for p in loading.pics:
			if (prep):
				if (prep == 1):
					p.mousePressEvent(event)
				elif (prep == 2):
					p.mouseReleaseEvent(event, k=loading.k)
				elif (prep == 3):
					p.mouseDoubleClickEvent(event)
				elif (prep == 4):
					p.resizeEvent(event)
			else:
				p.wheelEvent(event)

#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeni onformaci o fotografii metadata, obrazova data
class InfBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self,ims,prep):
		super().__init__()

		if not(len(ims)):
			return

		s=getFileName(ims[0])
		for im in ims[1:]:
			s+=', '
			s+=getFileName(im)

		self.title = s
		self.setWindowIcon(self.style().standardIcon(QStyle.SP_FileDialogInfoView))
		self.ims=ims
		self.prep=prep
		
		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		layout=QHBoxLayout(self)
		layoutV=QVBoxLayout(self)
		lv=QVBoxLayout(self)
		lh=QHBoxLayout(self)
		lh2=QHBoxLayout(self)

		layout.setContentsMargins(0, 0, 0, 0)
		layoutV.setContentsMargins(2, 0, 2, 0)
		lv.setContentsMargins(0, 0, 0, 0)
		lh.setContentsMargins(0, 2, 0, 0)
		lh2.setContentsMargins(2, 0, 2, 0)

		pics=[]

		for im in self.ims:
			strCh=""
			h=0

			lab=QLabel(getFileName(im),self)
			lab.setFont(f3)
			lab.setAlignment(Qt.AlignCenter)
			lab.setMaximumHeight(30)

			lv.addWidget(lab)

			pic=QtImageViewer(self,im)
			pic.setMinimumSize(600,470)
			pics.append(pic)

			lv.addWidget(pic)

			if(sett.compWithExif or self.prep):
				scrollArea = QScrollArea(self)


				for k, v in getInfExifData(im).items():
					strCh+=str(k)+": "+str(v)+"\n"
					h+=20

				labelInf = QLabel(strCh, self)
				labelInf.setFont(f2)
				labelInf.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
				labelInf.width = 550
				labelInf.height = 105 + h
				labelInf.setAlignment(Qt.AlignCenter)

				scroLayoutV = QVBoxLayout()
				scroLayoutV.addWidget(labelInf)
				widget=QWidget()
				widget.setLayout(scroLayoutV)
				scrollArea.setWidget(widget)

				scrollArea.setMinimumWidth(600)
				scrollArea.setMaximumHeight(300)
				scrollArea.setWidgetResizable(True)

				lv.addWidget(scrollArea)


			tmp = QWidget()
			tmp.setLayout(lv)
			layout.addWidget(tmp)

			layoutV = QVBoxLayout(self)
			lv = QVBoxLayout(self)
			lh = QHBoxLayout(self)
			lh2 = QHBoxLayout(self)
			layoutV.setContentsMargins(2, 0, 2, 0)
			lv.setContentsMargins(0, 0, 0, 0)
			lh.setContentsMargins(0, 2, 0, 0)
			lh2.setContentsMargins(2, 0, 2, 0)


		loading.pics=pics
		for p in loading.pics:
			loading.picsZ.append(p)

		btnAccept = QPushButton("Zavřít", self)
		btnAccept.setFont(f)
		btnAccept.clicked.connect(self.close)

		tmp = QWidget()
		tmp.setLayout(layout)
		layoutV.addWidget(tmp)

		lh.addStretch()
		lh.addWidget(btnAccept)
		lh.addStretch()

		tmp = QWidget()
		tmp.setLayout(lh)
		layoutV.addWidget(tmp)

		self.setLayout(layoutV)

		self.setGeometry(600, 700, 600, 700)
		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())

		if(len(self.ims)>1):
			if(sett.fullScreen):
				self.showFullScreen()
			else:
				self.showMaximized()
		else:
			self.show()
		#--------------------------------------------------------------------------------------------
		# Metoda ktera se vykona automaticky pri ukonceni komponenty
		def closeEvent(self, event):
			windows.list.remove(self)
			
			event.accept()
#--------------------------------------------------------------------------------------------
#Trida pro okno, kvuli potvrzeni uzivatele, smazani duplikaci,...
class YesNoDupBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self,dupWindow,dupls,prep):
		super().__init__()
		self.title = 'Upozornění'
		self.setWindowIcon(self.style().standardIcon(QStyle.SP_MessageBoxWarning))
		self.dupls=dupls
		self.dupWindow=dupWindow

		self.width=540
		self.height=72

		self.prep=prep


		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		#print(self.dupls)

		t=""
		if(self.prep):
			if (self.prep==2):
				t += "aktuálně "

			t = "ignorované"
		else:
			t += "neoznačené"

		label = QLabel("Opravdu si přejete odstranit "+t+" fotografie?", self)
		label.setFont(f2)
		label.setAlignment(Qt.AlignCenter)
		label.setFixedSize(self.width-5,self.height-38)
		
		self.setFixedSize(self.width,self.height)

		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())
		
		btnAccept = QPushButton("Potvrdit", self)
		btnAccept.setFont(f)
		btnAccept.clicked.connect(self.delDups)

		if not(self.prep):
			btnAccept.clicked.connect(self.dupWindow.close)
			btnAccept.clicked.connect(self.close)
		else:
			btnAccept.clicked.connect(self.close)

		btnAccept.move(self.width/2-218,self.height-30)
		
		btnCancel = QPushButton("Zrušit", self)
		btnCancel.setFont(f)
		btnCancel.clicked.connect(self.close)
		btnCancel.move(self.width/2+120,self.height-30)
		
		self.show()
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		windows.list.remove(self)
		
		event.accept()
	#--------------------------------------------------------------------------------------------
	# Metoda pro vymazani nevybranych fotografii (nevybranych pro ponechani)
	def delDups(self):
		c,self.dupls = delDups(self.dupls, sett.toRecBin)

		strCh = "Fotografie byly"

		if (sett.toRecBin):
			strDel = "přesunuty do koše."
		else:
			strDel = "odstraněny."

		text=strCh + " " + strDel + " (" + xstr(c) + ")"

		showNotBox(text,"Oznámení",self.prep)

		if(self.prep==2):
			self.dupWindow.updateDupls(self.dupls)

			self.dupWindow.baseHlayouts = []

			for i in reversed(range(self.dupWindow.baseHlayout.count())):
				self.dupWindow.baseHlayout.itemAt(i).widget().deleteLater()

			self.dupWindow.qb = []
			self.dupWindow.btnInf = []
			self.dupWindow.labelPics = []
			self.dupWindow.labels = []
			self.dupWindow.labelsPath = []
			self.dupWindow.labelsInf = []
			self.dupWindow.labelsPerc = []
			self.dupWindow.txt = []
			self.dupWindow.boxLocks = []

			for i, x in enumerate(self.dupWindow.indexes):
				self.dupWindow.baseHlayouts.append(self.dupWindow.gener(i, x))

			for l in self.dupWindow.baseHlayouts:
				tmp = QWidget()
				tmp.setLayout(l)
				self.dupWindow.baseHlayout.addWidget(tmp)
#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeni a modifikaci nastaveni aplikace
class SettingsBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self):
		super().__init__()
		self.title = 'Nastavení'
		self.setWindowIcon(QIcon('./resources/settings-icon.png'))
		self.width=520
		self.height=580

		self.comboBoxBin= QComboBox(self)
		self.comboBoxpreferenceMeasure= QComboBox(self)
		self.comboBoxpreferenceMeasureNeg= QComboBox(self)
		self.comboBoxpreferenceOnlyOne= QComboBox(self)
		self.comboBoxSubFolders= QComboBox(self)
		self.comboBoxSizeOfImage= QComboBox(self)
		self.comboBoxComplDupl= QComboBox(self)
		self.comboBoxOnlyDupl= QComboBox(self)
		self.comboBoxCompOnlyFileName= QComboBox(self)
		self.comboBoxQmeasure= QComboBox(self)
		self.comboBoxStats = QComboBox(self)
		self.comboBoxFullScreen = QComboBox(self)
		self.comboBoxSortGrups = QComboBox(self)
		self.comboBoxCompWithExif = QComboBox(self)

		self.qCheckBoxQmeasureBlur=QCheckBox(self)
		self.qCheckBoxQmeasureNoise=QCheckBox(self)
		self.qCheckBoxQmeasureColorFullness=QCheckBox(self)

		self.textEditTimeGap = QLineEdit(self)
		self.textEditNumOfGroups = QLineEdit(self)

		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)
		labelW=240
		comboW=90
		firstH=-280

		self.setFixedSize(self.width,self.height)
		
		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())
		
		labelBin = QLabel("Vymazat", self)
		labelBin.setFont(f)
		labelBin.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelBin.move(self.width / 2-labelW, self.height/2+firstH+5)
		
		self.comboBoxBin.addItem('Trvale')
		self.comboBoxBin.addItem('Do koše')
		self.comboBoxBin.setFont(f)
		self.comboBoxBin.move(self.width / 2+comboW, self.height/2+firstH)
		self.comboBoxBin.setCurrentIndex(sett.toRecBin)

		firstH+=30

		labelpreferenceMeasure = QLabel("Preference označení", self)
		labelpreferenceMeasure.setFont(f)
		labelpreferenceMeasure.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelpreferenceMeasure.move(self.width / 2 - labelW, self.height / 2 +firstH+18)
		
		self.comboBoxpreferenceMeasure.addItem('Nic')
		self.comboBoxpreferenceMeasure.addItem('Všechny')
		self.comboBoxpreferenceMeasure.addItem('Velikost')
		self.comboBoxpreferenceMeasure.addItem('Datum modifikace')
		self.comboBoxpreferenceMeasure.addItem('Datum vytvoření')
		self.comboBoxpreferenceMeasure.addItem('Složka')
		self.comboBoxpreferenceMeasure.addItem('Rozlišení')
		self.comboBoxpreferenceMeasure.addItem('Percentil kvality')
		self.comboBoxpreferenceMeasure.setFont(f)
		self.comboBoxpreferenceMeasure.currentIndexChanged.connect(self.prefChanged)
		self.comboBoxpreferenceMeasure.move(self.width / 2+comboW, self.height / 2 +firstH)
		self.comboBoxpreferenceMeasure.setCurrentIndex(sett.preferenceMeasure)

		firstH += 30

		self.comboBoxpreferenceMeasureNeg.addItem('Sestupně')
		self.comboBoxpreferenceMeasureNeg.addItem('Vzestupně')
		self.comboBoxpreferenceMeasureNeg.setFont(f)
		self.comboBoxpreferenceMeasureNeg.move(self.width / 2+comboW, self.height / 2 +firstH)
		self.comboBoxpreferenceMeasureNeg.setCurrentIndex(sett.preferenceMeasureNeg)

		firstH+=30

		labelPreferenceMeasureOnlyOne = QLabel("Označit pouze jednu", self)
		labelPreferenceMeasureOnlyOne.setFont(f)
		labelPreferenceMeasureOnlyOne.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelPreferenceMeasureOnlyOne.move(self.width / 2 - labelW, self.height / 2 +firstH)

		self.comboBoxpreferenceOnlyOne.addItem('Ne')
		self.comboBoxpreferenceOnlyOne.addItem('Ano')
		self.comboBoxpreferenceOnlyOne.setFont(f)
		self.comboBoxpreferenceOnlyOne.move(self.width / 2+comboW, self.height / 2 +firstH)
		self.comboBoxpreferenceOnlyOne.setCurrentIndex(sett.preferenceMeasureOnlyOne)

		firstH+=30

		labelSizeOfImage = QLabel("Rozlišení fotografie podle metadat", self)
		labelSizeOfImage.setFont(f)
		labelSizeOfImage.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelSizeOfImage.move(self.width / 2 - labelW, self.height / 2 +firstH)
		
		self.comboBoxSizeOfImage.addItem('Ne')
		self.comboBoxSizeOfImage.addItem('Ano')
		self.comboBoxSizeOfImage.setFont(f)
		self.comboBoxSizeOfImage.move(self.width / 2+comboW, self.height / 2 +firstH)
		self.comboBoxSizeOfImage.setCurrentIndex(sett.sizeOfImage)

		firstH += 30

		labelComplDupl = QLabel("Úplná duplikace", self)
		labelComplDupl.setFont(f)
		labelComplDupl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelComplDupl.move(self.width / 2 - labelW, self.height / 2 +firstH)
		
		self.comboBoxComplDupl.addItem('Ne')
		self.comboBoxComplDupl.addItem('Ano')
		self.comboBoxComplDupl.setFont(f)
		self.comboBoxComplDupl.move(self.width / 2+comboW, self.height / 2 +firstH)
		self.comboBoxComplDupl.setCurrentIndex(sett.completeDupl)

		firstH += 30

		labelOnlyDupl = QLabel("Zobrazit pouze duplikace", self)
		labelOnlyDupl.setFont(f)
		labelOnlyDupl.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelOnlyDupl.move(self.width / 2 - labelW, self.height / 2 +firstH)
		
		self.comboBoxOnlyDupl.addItem('Ne')
		self.comboBoxOnlyDupl.addItem('Ano')
		self.comboBoxOnlyDupl.setFont(f)
		self.comboBoxOnlyDupl.move(self.width / 2 + comboW, self.height / 2 +firstH)
		self.comboBoxOnlyDupl.setCurrentIndex(sett.onlyDupl)

		firstH += 30

		labelTimeGap = QLabel("Časová mezera", self)
		labelTimeGap.setFont(f)
		labelTimeGap.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelTimeGap.move(self.width / 2 - labelW, self.height / 2 +firstH+6)

		self.textEditTimeGap.setFont(f)
		self.textEditTimeGap.move(self.width / 2 + comboW, self.height / 2 +firstH)
		self.textEditTimeGap.setFixedSize(50,30)
		self.textEditTimeGap.setText(str(sett.timeGap))
		self.textEditTimeGap.textChanged.connect(self.txtChanged)
		self.textEditTimeGap.setAlignment(Qt.AlignCenter)

		firstH += 38

		labelSubFolders = QLabel("Podsložky", self)
		labelSubFolders.setFont(f)
		labelSubFolders.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelSubFolders.move(self.width / 2 - labelW, self.height / 2 +firstH)

		self.comboBoxSubFolders.addItem('Ne')
		self.comboBoxSubFolders.addItem('Ano')
		self.comboBoxSubFolders.setFont(f)
		self.comboBoxSubFolders.move(self.width / 2 + comboW, self.height / 2 +firstH)
		self.comboBoxSubFolders.setCurrentIndex(sett.subFolders)

		firstH += 30

		labelCompOnlyFileName = QLabel("Porovnat názvy fotografií", self)
		labelCompOnlyFileName.setFont(f)
		labelCompOnlyFileName.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelCompOnlyFileName.move(self.width / 2 - labelW, self.height / 2 + firstH)

		self.comboBoxCompOnlyFileName.addItem('Ne')
		self.comboBoxCompOnlyFileName.addItem('Ano')
		self.comboBoxCompOnlyFileName.setFont(f)
		self.comboBoxCompOnlyFileName.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxCompOnlyFileName.setCurrentIndex(sett.compOnlyFileName)

		firstH += 30

		labelQmeasure = QLabel("Vyhodnotit kvalitu fotografií", self)
		labelQmeasure.setFont(f)
		labelQmeasure.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelQmeasure.move(self.width / 2 - labelW, self.height / 2 + firstH+3)

		self.comboBoxQmeasure.addItem('Ne')
		self.comboBoxQmeasure.addItem('Ano')
		self.comboBoxQmeasure.setFont(f)
		self.comboBoxQmeasure.currentIndexChanged.connect(self.qMeasureChanged)
		self.comboBoxQmeasure.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxQmeasure.setCurrentIndex(sett.qMeasure)

		firstH += 30

		self.qCheckBoxQmeasureBlur.setText("Ostrost")
		self.qCheckBoxQmeasureBlur.setChecked(sett.qMeasureParametres[0])
		self.qCheckBoxQmeasureBlur.move(self.width / 2 - labelW+20, self.height / 2 + firstH)
		self.qCheckBoxQmeasureBlur.setFont(f)
		self.qCheckBoxQmeasureNoise.setText("Šum")
		self.qCheckBoxQmeasureNoise.setChecked(sett.qMeasureParametres[1])
		self.qCheckBoxQmeasureNoise.move(self.width / 2 - labelW + 120, self.height / 2 + firstH)
		self.qCheckBoxQmeasureNoise.setFont(f)
		self.qCheckBoxQmeasureColorFullness.setText("Barevnost")
		self.qCheckBoxQmeasureColorFullness.setChecked(sett.qMeasureParametres[2])
		self.qCheckBoxQmeasureColorFullness.move(self.width / 2 - labelW + 200, self.height / 2 + firstH)
		self.qCheckBoxQmeasureColorFullness.setFont(f)

		if not(sett.qMeasure):
			self.qCheckBoxQmeasureBlur.setDisabled(True)
			self.qCheckBoxQmeasureNoise.setDisabled(True)
			self.qCheckBoxQmeasureColorFullness.setDisabled(True)

		firstH += 30

		labelStats = QLabel("Zobrazit statistiky", self)
		labelStats.setFont(f)
		labelStats.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelStats.move(self.width / 2 - labelW, self.height / 2 + firstH+5)

		self.comboBoxStats.addItem('Ne')
		self.comboBoxStats.addItem('Ano')
		self.comboBoxStats.setFont(f)
		self.comboBoxStats.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxStats.setCurrentIndex(sett.stats)

		firstH += 30

		labelNumOfGroups = QLabel("Počet skupin duplikací", self)
		labelNumOfGroups.setFont(f)
		labelNumOfGroups.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelNumOfGroups.move(self.width / 2 - labelW, self.height / 2 + firstH + 8)

		self.textEditNumOfGroups.setFont(f)
		self.textEditNumOfGroups.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.textEditNumOfGroups.setFixedSize(50, 30)
		self.textEditNumOfGroups.setText(str(sett.numOfGroups))
		self.textEditNumOfGroups.textChanged.connect(self.txtChanged2)
		self.textEditNumOfGroups.setAlignment(Qt.AlignCenter)

		firstH += 38

		labelFullScreen = QLabel("Zobrazení na celou obrazovku", self)
		labelFullScreen.setFont(f)
		labelFullScreen.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelFullScreen.move(self.width / 2 - labelW, self.height / 2 + firstH+5)

		self.comboBoxFullScreen.addItem('Ne')
		self.comboBoxFullScreen.addItem('Ano')
		self.comboBoxFullScreen.setFont(f)
		self.comboBoxFullScreen.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxFullScreen.setCurrentIndex(sett.fullScreen)

		firstH += 30

		labelSortGrups = QLabel("Seřadit skupiny podle", self)
		labelSortGrups.setFont(f)
		labelSortGrups.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelSortGrups.move(self.width / 2 - labelW, self.height / 2 + firstH+5)

		self.comboBoxSortGrups.addItem('Čas')
		self.comboBoxSortGrups.addItem('Počet fotografií')
		self.comboBoxSortGrups.setFont(f)
		self.comboBoxSortGrups.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxSortGrups.setCurrentIndex(sett.sortGrups)

		firstH += 30

		labelCompWithExif = QLabel("Zobrazit metadata v paralelním porovnání", self)
		labelCompWithExif.setFont(f)
		labelCompWithExif.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
		labelCompWithExif.move(self.width / 2 - labelW, self.height / 2 + firstH + 5)

		self.comboBoxCompWithExif.addItem('Ne')
		self.comboBoxCompWithExif.addItem('Ano')
		self.comboBoxCompWithExif.setFont(f)
		self.comboBoxCompWithExif.move(self.width / 2 + comboW, self.height / 2 + firstH)
		self.comboBoxCompWithExif.setCurrentIndex(sett.compWithExif)





		btnAccept = QPushButton("Potvrdit", self)
		btnAccept.setFont(f)
		btnAccept.setIcon(self.style().standardIcon(QStyle.SP_DialogApplyButton))
		btnAccept.clicked.connect(self.saveSett)
		btnAccept.clicked.connect(self.close)
		btnAccept.move(self.width/2-160,self.height-35)
		
		btnReset = QPushButton("Resetovat", self)
		btnReset.setFont(f)
		btnReset.setIcon(self.style().standardIcon(QStyle.SP_DialogCancelButton))
		btnReset.clicked.connect(self.resetSett)
		btnReset.move(self.width / 2 -56, self.height - 35)
		
		btnCancel = QPushButton("Zrušit", self)
		btnCancel.setFont(f)
		btnCancel.clicked.connect(self.close)
		btnCancel.move(self.width / 2+58,self.height-35)

		self.comboBoxBin.setToolTip("Nastavení cíle mazání fotografií.")
		self.comboBoxpreferenceMeasure.setToolTip("Nastavení preference zaškrtnutí fotografie v duplikační skupině.")
		self.comboBoxpreferenceMeasureNeg.setToolTip("Nastavení řazení při uplatnění preference.")
		self.comboBoxpreferenceOnlyOne.setToolTip("Nastavení toho zda v případě shodné hodnoty preference, má být vybrána pouze první nebo všechny se zvolenou hodnotou.")
		self.comboBoxSubFolders.setToolTip("Nastavení procházení podsložek. (zde ma vyšší váhu checkBox u každé složky)")
		self.comboBoxSizeOfImage.setToolTip("Nastavení toho zda do porovnání fotografií jsou zahrnuty i údaje o rozlišení z metadat.")
		self.comboBoxComplDupl.setToolTip("Nastavení toho zda mají být porovnána všechna metadata při určení duplikace.")
		self.comboBoxOnlyDupl.setToolTip("Nastavení toho zda se mají zobrazit pouze skupiny, které mají alespoň jednu fotografii, tedy byla nalezena duplikace.")
		self.comboBoxCompOnlyFileName.setToolTip("Nastavení toho zda stejný název fotografií znamená, že jsou duplicitní.")
		self.comboBoxQmeasure.setToolTip("Nastavení toho zda má být vyhodnocena kvalita fotografií.")
		self.comboBoxStats.setToolTip("Nastavení zobrazení statistik vyhledání po jeho skončení.")
		self.comboBoxFullScreen.setToolTip("Nastavení toho zda se mají okna zobrazovat na celou obrazovku.")
		self.comboBoxSortGrups.setToolTip("Nastavení řazeni duplikačních skupin.")
		self.comboBoxCompWithExif.setToolTip("Nastavení toho zda při paralelním porovnání fotografií mají být zobrazena i jejich metadata.")

		self.qCheckBoxQmeasureBlur.setToolTip("Aktivuje položku rozmazání při vyhodnocení kvality.")
		self.qCheckBoxQmeasureNoise.setToolTip("Aktivuje položku šum při vyhodnocení kvality.")
		self.qCheckBoxQmeasureColorFullness.setToolTip("Aktivuje položku barevnost při vyhodnocení kvality.")

		self.textEditTimeGap.setToolTip("Nastavení maximálního časového rozestupu duplicitních fotografií.")
		self.textEditNumOfGroups.setToolTip("Nastavení počtu, najednou zobrazených duplikačních skupin.")

		self.show()
	#--------------------------------------------------------------------------------------------
	def qMeasureChanged(self):
		if(not(self.comboBoxQmeasure.currentIndex()) and self.comboBoxpreferenceMeasure.currentIndex()==7):
			self.comboBoxpreferenceMeasure.setCurrentIndex(0)

		if not(self.comboBoxQmeasure.currentIndex()):
			self.qCheckBoxQmeasureBlur.setDisabled(True)
			self.qCheckBoxQmeasureNoise.setDisabled(True)
			self.qCheckBoxQmeasureColorFullness.setDisabled(True)

			self.qCheckBoxQmeasureBlur.setChecked(False)
			self.qCheckBoxQmeasureNoise.setChecked(False)
			self.qCheckBoxQmeasureColorFullness.setChecked(False)
		else:
			self.qCheckBoxQmeasureBlur.setDisabled(False)
			self.qCheckBoxQmeasureNoise.setDisabled(False)
			self.qCheckBoxQmeasureColorFullness.setDisabled(False)

			self.qCheckBoxQmeasureBlur.setChecked(sett.qMeasureParametres[0])
			self.qCheckBoxQmeasureNoise.setChecked(sett.qMeasureParametres[1])
			self.qCheckBoxQmeasureColorFullness.setChecked(sett.qMeasureParametres[2])

			if(not(self.qCheckBoxQmeasureBlur.isChecked()) and not(self.qCheckBoxQmeasureNoise.isChecked()) and not(self.qCheckBoxQmeasureColorFullness.isChecked())):
				self.qCheckBoxQmeasureBlur.setChecked(True)
				sett.qMeasureParametres[0]=True

	#--------------------------------------------------------------------------------------------
	def prefChanged(self):
		if(self.comboBoxpreferenceMeasure.currentIndex()==7):
			self.comboBoxQmeasure.setCurrentIndex(1)
	#--------------------------------------------------------------------------------------------
	def txtChanged(self):
		try:
			c=int(self.textEditTimeGap.text())

			if(c<0 or c>100000):
				self.textEditTimeGap.setText(str(0))
		except ValueError:
			self.textEditTimeGap.setText(str(0))
			pass
	#--------------------------------------------------------------------------------------------
	def txtChanged2(self):
		try:
			c=int(self.textEditNumOfGroups.text())

			if(c<1) or (c>4):
				self.textEditNumOfGroups.setText(str(1))
		except ValueError:
			self.textEditNumOfGroups.setText(str(1))
			pass
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		windows.list.remove(self)
		
		event.accept()
	#--------------------------------------------------------------------------------------------
	# Metoda pro aktualizaci hodnot nastaveni a jeho nasledne ulozeni
	def saveSett(self):
		sett.toRecBin=self.comboBoxBin.currentIndex()
		sett.preferenceMeasure = self.comboBoxpreferenceMeasure.currentIndex()
		sett.subFolders = self.comboBoxSubFolders.currentIndex()
		sett.sizeOfImage = self.comboBoxSizeOfImage.currentIndex()
		sett.preferenceMeasureNeg =  self.comboBoxpreferenceMeasureNeg.currentIndex()
		sett.completeDupl = self.comboBoxComplDupl.currentIndex()
		sett.onlyDupl = self.comboBoxOnlyDupl.currentIndex()
		sett.preferenceMeasureOnlyOne = self.comboBoxpreferenceOnlyOne.currentIndex()
		sett.compOnlyFileName = self.comboBoxCompOnlyFileName.currentIndex()
		sett.qMeasure=self.comboBoxQmeasure.currentIndex()
		sett.stats=self.comboBoxStats.currentIndex()
		sett.fullScreen=self.comboBoxFullScreen.currentIndex()
		sett.sortGrups=self.comboBoxSortGrups.currentIndex()
		sett.compWithExif=self.comboBoxCompWithExif.currentIndex()


		sett.qMeasureParametres[0]=self.qCheckBoxQmeasureBlur.isChecked()
		sett.qMeasureParametres[1]=self.qCheckBoxQmeasureNoise.isChecked()
		sett.qMeasureParametres[2]=self.qCheckBoxQmeasureColorFullness.isChecked()

		try:
			sett.timeGap = int(self.textEditTimeGap.text())
		except ValueError:
			sett.timeGap = 0

		try:
			sett.numOfGroups = int(self.textEditNumOfGroups.text())
		except ValueError:
			self.textEditNumOfGroups.setText(str(1))
			sett.numOfGroups = 1

		saveSettings(sett)
	#--------------------------------------------------------------------------------------------
	# Metoda pro reset nastaveni do defaultniho nastaveni aplikace
	def resetSett(self):
		self.comboBoxBin.setCurrentIndex(True)
		self.comboBoxpreferenceMeasure.setCurrentIndex(0)
		self.comboBoxpreferenceMeasureNeg.setCurrentIndex(True)
		self.comboBoxSubFolders.setCurrentIndex(True)
		self.comboBoxSizeOfImage.setCurrentIndex(False)
		self.comboBoxComplDupl.setCurrentIndex(False)
		self.comboBoxOnlyDupl.setCurrentIndex(True)
		self.textEditTimeGap.setText(str(3))
		self.comboBoxpreferenceOnlyOne.setCurrentIndex(True)
		self.comboBoxCompOnlyFileName.setCurrentIndex(False)
		self.comboBoxQmeasure.setCurrentIndex(False)

		self.qCheckBoxQmeasureBlur.setChecked(False)
		self.qCheckBoxQmeasureNoise.setChecked(False)
		self.qCheckBoxQmeasureColorFullness.setChecked(False)

		self.comboBoxStats.setCurrentIndex(False)
		self.textEditNumOfGroups.setText(str(2))
		self.comboBoxFullScreen.setCurrentIndex(True)
		self.comboBoxSortGrups.setCurrentIndex(0)
		self.comboBoxCompWithExif.setCurrentIndex(False)
		#step=1

		sett = Settings(True, 0, True, False, False, False, True, 5, True, True, False, [False, False, False], False, 2, True, 0, False, 1)
		saveSettings(sett)
#--------------------------------------------------------------------------------------------
#Trida pro okno na modifikaci filtru, atim pouze datumovy interval
class FiltersBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self):
		super().__init__()
		self.title = 'Filtry'
		self.setWindowIcon(QIcon('./resources/filters-icon.png'))
		self.width = 460
		self.height = 550
		self.qCh=QCheckBox(self)
		self.fromCalendar=QCalendarWidget(self)
		self.toCalendar =QCalendarWidget(self)
		
		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		self.setFixedSize(self.width, self.height)
		
		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())
		
		self.qCh.move(self.width / 2 - self.width/2 +10, self.height/2-40)
		self.qCh.setText("Zahrnout")
		self.qCh.setFont(f)
						
		calendarHeight=260
		calendarWidth=170
		
		self.fromCalendar.setGridVisible(True)
		self.fromCalendar.move(self.width / 2-calendarWidth, self.height/2-calendarHeight)
		self.fromCalendar.setMinimumDate(QDate(1900, 1, 1))
		self.fromCalendar.setMaximumDate(QDate.currentDate())
		self.fromCalendar.setFirstDayOfWeek(Qt.Monday)
		self.fromCalendar.setFixedSize(350,220)
		
		self.toCalendar.setGridVisible(True)
		self.toCalendar.move(self.width / 2-calendarWidth, self.height/2-calendarHeight+240 )
		self.toCalendar.setMinimumDate(QDate(1900, 1, 1))
		self.toCalendar.setMaximumDate(QDate.currentDate())
		self.toCalendar.setFirstDayOfWeek(Qt.Monday)
		self.toCalendar.setFixedSize(350, 220)
		
		btnAccept = QPushButton("Přidat", self)
		btnAccept.setFont(f)
		btnAccept.setIcon(self.style().standardIcon(QStyle.SP_DialogApplyButton))
		btnAccept.clicked.connect(self.saveFilters)
		btnAccept.move(self.width / 2 -205, self.height - 35)

		btnReset = QPushButton("Resetovat", self)
		btnReset.setFont(f)
		btnReset.setIcon(self.style().standardIcon(QStyle.SP_DialogCancelButton))
		btnReset.clicked.connect(self.resetFilters)
		btnReset.move(self.width / 2 - 110, self.height - 35)

		btnView = QPushButton("Zobrazit filtry", self)
		btnView.setFont(f)
		btnView.clicked.connect(self.viewFilters)
		btnView.move(self.width / 2, self.height - 35)

		btnCancel = QPushButton("Zrušit", self)
		btnCancel.setFont(f)
		btnCancel.clicked.connect(self.close)
		btnCancel.move(self.width / 2 + 125, self.height - 35)
		
		self.show()
	#--------------------------------------------------------------------------------------------
	def viewFilters(self):
		if not(len(filters.dateList)):
			s="\nŽádné filty nebyly nastaveny.\nZvolené operace budou probíhat pro všechny nalezené fotografie.\n"
		else:
			s = ""

			for f in filters.dateList:
				s+=str(f)+'\n'

		showNotBox(s,"Nastavené filtry",1)
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		windows.list.remove(self)
		
		event.accept()
	#--------------------------------------------------------------------------------------------
	# Metoda pro vymazani vsech vytvorenych filtru
	def resetFilters(self):
		global filters
		
		filters = Filters()
		
		self.close()
	#--------------------------------------------------------------------------------------------
	# Metoda pro ulozeni nove vytvoreneho filtru
	def saveFilters(self):
		incD=self.qCh.checkState()
		fromD=self.fromCalendar.selectedDate()
		toD=D=self.toCalendar.selectedDate()

		if(fromD>toD):
			return

		date=DateInterval(fromD,toD,incD)
		
		filters.dateList.append(date)
		
		self.close()
#--------------------------------------------------------------------------------------------
#
class DateDialog(QDialog):
	def __init__(self, parent = None):
		super(DateDialog, self).__init__(parent)

		layoutH = QHBoxLayout(self)
		layoutH.setContentsMargins(2, 0, 2, 0)
		layoutV = QVBoxLayout(self)
		layoutV.setContentsMargins(2, 0, 2, 0)
		layoutH2 = QHBoxLayout(self)
		layoutH2.setContentsMargins(2, 0, 2, 0)


		time = "00"

		self.setFixedSize(500,130)

		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())

		l = QLabel("Počet dnů",self)
		l.setAlignment(Qt.AlignCenter)
		l1 = QLabel("Počet měsíců", self)
		l1.setAlignment(Qt.AlignCenter)
		l2 = QLabel("Počet roků", self)
		l2.setAlignment(Qt.AlignCenter)
		l3 = QLabel("Počet hodin", self)
		l3.setAlignment(Qt.AlignCenter)
		l4 = QLabel("Počet minut", self)
		l4.setAlignment(Qt.AlignCenter)
		l5 = QLabel("Počet sekund", self)
		l5.setAlignment(Qt.AlignCenter)


		self.textDays=QLineEdit(time,self)
		self.textDays.setAlignment(Qt.AlignCenter)
		self.textDays.setValidator(QIntValidator(0, 1000,self))

		self.textMonths=QLineEdit(time,self)
		self.textMonths.setAlignment(Qt.AlignCenter)
		self.textMonths.setValidator(QIntValidator(0, 1000, self))

		self.textYears=QLineEdit(time+time, self)
		self.textYears.setAlignment(Qt.AlignCenter)
		self.textYears.setValidator(QIntValidator(0, 10000, self))

		self.textHours=QLineEdit(time, self)
		self.textHours.setAlignment(Qt.AlignCenter)
		self.textHours.setValidator(QIntValidator(0, 1000, self))

		self.textMinutes=QLineEdit(time, self)
		self.textMinutes.setAlignment(Qt.AlignCenter)
		self.textMinutes.setValidator(QIntValidator(0, 1000, self))

		self.textSeconds=QLineEdit(time, self)
		self.textSeconds.setAlignment(Qt.AlignCenter)
		self.textSeconds.setValidator(QIntValidator(0, 1000, self))

		layoutH.addWidget(self.textDays)
		layoutH2.addWidget(l)

		layoutH.addWidget(self.textMonths)
		layoutH2.addWidget(l1)

		layoutH.addWidget(self.textYears)
		layoutH2.addWidget(l2)

		layoutH.addWidget(self.textHours)
		layoutH2.addWidget(l3)

		layoutH.addWidget(self.textMinutes)
		layoutH2.addWidget(l4)

		layoutH.addWidget(self.textSeconds)
		layoutH2.addWidget(l5)

		tmp = QWidget()
		tmp.setLayout(layoutH2)
		layoutV.addWidget(tmp)

		tmp = QWidget()
		tmp.setLayout(layoutH)
		layoutV.addWidget(tmp)

		buttons = QDialogButtonBox(
			QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
			Qt.Horizontal, self)

		buttons.accepted.connect(self.accept)
		buttons.rejected.connect(self.reject)

		buttons.button(QDialogButtonBox.Cancel).setText("Zrušit")

		Rgroup = QButtonGroup(self)
		self.r0 = QRadioButton("Přidat")
		Rgroup.addButton(self.r0)
		self.r1 = QRadioButton("Odečíst")
		Rgroup.addButton(self.r1)

		self.r0.setChecked(True)

		layoutH = QHBoxLayout(self)
		layoutH.setContentsMargins(2, 0, 2, 0)

		layoutH.addStretch()
		layoutH.addWidget(self.r0)
		layoutH.addWidget(self.r1)
		layoutH.addStretch()

		tmp = QWidget()
		tmp.setLayout(layoutH)
		layoutV.addWidget(tmp)

		layoutH = QHBoxLayout(self)
		layoutH.setContentsMargins(2, 0, 2, 0)

		layoutH.addStretch()
		layoutH.addWidget(buttons)
		layoutH.addStretch()


		tmp = QWidget()
		tmp.setLayout(layoutH)
		layoutV.addWidget(tmp)

		self.setLayout(layoutV)
		self.setWindowTitle("Editace datumu")
	#--------------------------------------------------------------------------------------------
	def dateTime(self):
		try:
			return [int(self.textDays.text()), int(self.textMonths.text()), int(self.textYears.text()), int(self.textHours.text()), int(self.textMinutes.text()), int(self.textSeconds.text())]
		except:
			return[]
	#--------------------------------------------------------------------------------------------
	def prep(self):
		return self.r0.isChecked()
	#--------------------------------------------------------------------------------------------
	@staticmethod
	def getDateTime(parent = None):
		dialog = DateDialog(parent)
		res = dialog.exec_()
		date = dialog.dateTime()
		prep=dialog.prep()

		if res:
			return (date, prep)
		else:
			return False
#--------------------------------------------------------------------------------------------
#Trida pro okno na zobrazeni skupin duplikaci, vyber, export
class DuplsBox(QWidget):
	# Metoda pro inicializaci komponenty
	def __init__(self,dupls,ignored):
		super().__init__()
		self.title = 'Duplikace'
		self.setWindowIcon(self.style().standardIcon(QStyle.SP_FileDialogListView))

		self.height=768
		self.width=1024
		self.dupls=dupls
		self.ignored=ignored

		self.qb=[]
		self.btnInf=[]
		self.labelPics=[]
		self.labels=[]
		self.labelsPath=[]
		self.labelsInf=[]
		self.labelsPerc=[]
		self.txt=[]
		self.boxLocks=[]

		self.indexes=[]
		self.locks=[]

		self.baseVlayout=QVBoxLayout(self)
		self.baseVlayout.setContentsMargins(1, 1, 1, 1)
		self.baseHlayout=QHBoxLayout(self)
		self.baseHlayout.setContentsMargins(1, 1, 1, 1)
		self.baseHlayouts=[]

		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		self.setGeometry(self.width, self.height, self.width, self.height)

		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())

		height=300
		h=7
		lh=QHBoxLayout(self)
		lh.setContentsMargins(0,0,1,0)

		if not(loading.editMet):
			btnAccept = QPushButton("Smazat neoznačené", self)
			btnExport = QPushButton("Exportovat označené", self)
			btnSolveActual = QPushButton("Smazat aktuálně neoznačené", self)
			btnCompMarked = QPushButton("Porovnat označené", self)

			btnIgnored= QPushButton("Zobrazit ignorované", self)
			btnIgnored.clicked.connect(self.ignor)
			btnIgnored.setMinimumSize(500, 35)
			btnIgnored.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			lh.addStretch()
			lh.addWidget(btnIgnored)
			lh.addStretch()

			if(sett.fullScreen):
				btnClose = QPushButton("Zavřít", self)
				btnClose.clicked.connect(self.close)
				btnClose.setMinimumHeight(35)
				btnClose.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

				lh.addWidget(btnClose)

			tmp = QWidget()
			tmp.setLayout(lh)
			self.baseVlayout.addWidget(tmp)

			lh = QHBoxLayout(self)
			lh.setContentsMargins(1, 1, 1, 1)

			lv = QVBoxLayout(self)
			lv.setContentsMargins(1, 1, 1, 1)

			btnNext = QPushButton(">",self)
			btnNext.clicked.connect(lambda: self.mvD(1,sett.step))
			btnNext.setFixedSize(41, height)
			btnNext.setStyleSheet("QPushButton {font-size: 40pt;font-weight: bold;color: #000000;background-color: #FFE1D6;}")

			shortcut = QShortcut(QKeySequence(Qt.Key_Right), self)
			shortcut.activated.connect(btnNext.click)

			btnUndo = QPushButton("<",self)
			btnUndo.clicked.connect(lambda: self.mvD(0,sett.step))
			btnUndo.setFixedSize(41, height)
			btnUndo.setStyleSheet("QPushButton {font-size: 40pt;font-weight: bold;color: #000000;background-color: #FFE1D6;}")

			shortcut = QShortcut(QKeySequence(Qt.Key_Left), self)
			shortcut.activated.connect(btnUndo.click)

			lv.addStretch()
			lv.addStretch()
			lv.addWidget(btnUndo)
			lv.addStretch()

			tmp = QWidget()
			tmp.setLayout(lv)
			lh.addWidget(tmp)

			self.baseHlayouts=[]

			if(len(self.dupls)):
				for i in range(0, sett.numOfGroups):
					r=0

					if (i):
						r=self.moveDup(1,0,sett.step)

					self.indexes.append(r)
					self.locks.append(False)

					self.baseHlayouts.append(self.gener(i,r))
			else:
				self.baseHlayouts.append(self.gener(0, 0))
				btnNext.hide()
				btnUndo.hide()
				btnAccept.setDisabled(True)
				btnExport.setDisabled(True)
				btnCompMarked.setDisabled(True)
				btnSolveActual.setDisabled(True)

			for l in self.baseHlayouts:
				tmp = QWidget()
				tmp.setLayout(l)
				self.baseHlayout.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(self.baseHlayout)
			lh.addWidget(tmp)

			lv = QVBoxLayout(self)
			lv.setContentsMargins(1, 1, 1, 1)

			lv.addStretch()
			lv.addStretch()
			lv.addWidget(btnNext)
			lv.addStretch()

			tmp = QWidget()
			tmp.setLayout(lv)
			lh.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(lh)
			self.baseVlayout.addWidget(tmp)

			lh = QHBoxLayout(self)
			lh.setContentsMargins(1, 1, 1, 1)

			btnAccept.clicked.connect(lambda: showYesNoDupBox(self,self.dupls,0))
			btnAccept.setMinimumSize(200,50)
			btnAccept.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			btnExport.clicked.connect(self.exportToZip)
			btnExport.setMinimumSize(280, 50)
			btnExport.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			btnSolveActual.clicked.connect(self.solveActual)
			btnSolveActual.setMinimumHeight(50)
			btnSolveActual.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			btnCompMarked.clicked.connect(self.compMarked)
			btnCompMarked.setMinimumSize(205,50)
			btnCompMarked.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			lh.addStretch()
			lh.addWidget(btnExport)
			lh.addWidget(btnCompMarked)
			lh.addWidget(btnAccept)
			lh.addWidget(btnSolveActual)
			lh.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh)
			self.baseVlayout.addWidget(tmp)
		else:
			if (sett.fullScreen):
				btnClose = QPushButton("Zavřít", self)
				btnClose.clicked.connect(self.close)
				btnClose.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

				lh.addStretch()
				lh.addWidget(btnClose)

				tmp = QWidget()
				tmp.setLayout(lh)
				self.baseVlayout.addWidget(tmp)

			lh = QHBoxLayout(self)
			lh.setContentsMargins(1, 1, 1, 1)

			self.baseHlayouts = []

			if (len(self.dupls)):
				r = 0

				self.indexes.append(r)
				self.locks.append(False)

				self.baseHlayouts.append(self.gener(0, r))
			else:
				self.baseHlayouts.append(self.gener(0, 0))

			for l in self.baseHlayouts:
				tmp = QWidget()
				tmp.setLayout(l)
				self.baseHlayout.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(self.baseHlayout)
			lh.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(lh)
			self.baseVlayout.addWidget(tmp)

			lh = QHBoxLayout(self)
			lh.setContentsMargins(1, 1, 1, 1)

			btnEditDate=QPushButton("Editovat datum", self)
			btnEditDate.clicked.connect(self.editMetDate)
			btnEditDate.setStyleSheet("QPushButton {font-size: 14pt;font-weight: bold;color: #000000;}")

			lh.addStretch()

			lh.addWidget(btnEditDate)


			lh.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh)
			self.baseVlayout.addWidget(tmp)

		if(sett.fullScreen):
			self.showFullScreen()
		else:
			self.showMaximized()

		if(sett.stats and not(loading.editMet)):
			self.stats()
	#--------------------------------------------------------------------------------------------
	def compMarked(self):
		d = []
		res=[]

		for i in self.indexes:
			d.append(self.dupls[i])

		for dupl in d:
			for ind, file in enumerate(dupl[1]):
				if (ind in dupl[0]):
					res.append(file)

		showInfBox(res,False)
	#--------------------------------------------------------------------------------------------
	def updateDupls(self,d):
		for ind,i in enumerate(self.indexes):
			#self.dupls[i][0]=[]
			self.dupls[i][1]=d[ind][1]
	#--------------------------------------------------------------------------------------------
	def solveActual(self):
		d=[]
		for i in self.indexes:
			d.append(self.dupls[i])

		showYesNoDupBox(self, d, 2)
	#--------------------------------------------------------------------------------------------
	def editMetDate(self):
		ret=DateDialog.getDateTime()

		if(ret):
			date=ret[0]
			prep=ret[1]

			print(prep, date)

			if(len(date)):
				for indDupl, dupl in enumerate(self.dupls):
					for index in dupl[0]:
						f = self.dupls[indDupl][1][index]

						chngDat(f, prep, date[0], date[1], date[2], date[3], date[4], date[5])
	#--------------------------------------------------------------------------------------------
	def mvD(self,prep,c):
		self.indexes = []

		for t in enumerate(self.txt):
			self.indexes.append(int(t[1].text()) - 1)

		self.baseHlayouts = []

		for i in reversed(range(self.baseHlayout.count())):
			self.baseHlayout.itemAt(i).widget().deleteLater()

		self.qb = []
		self.btnInf = []
		self.labelPics = []
		self.labels = []
		self.labelsPath = []
		self.labelsInf = []
		self.labelsPerc = []
		self.txt = []
		self.boxLocks=[]

		for i, x in enumerate(self.indexes):
			if not(self.locks[i]):
				r=self.moveDup(prep,x,c)
			else:
				r=x

			self.indexes[i]=r

			self.baseHlayouts.append(self.gener(i, r))

		for l in self.baseHlayouts:
			tmp = QWidget()
			tmp.setLayout(l)
			self.baseHlayout.addWidget(tmp)
	#--------------------------------------------------------------------------------------------
	def txtChanged(self,g):
		try:
			d=int(self.sender().text())-1

			if(0 <= d < len(self.dupls)):
				self.indexes[g]=d

				self.baseHlayouts = []

				for i in reversed(range(self.baseHlayout.count())):
					self.baseHlayout.itemAt(i).widget().deleteLater()

				self.qb = []
				self.btnInf = []
				self.labelPics = []
				self.labels = []
				self.labelsPath = []
				self.labelsInf = []
				self.labelsPerc = []
				self.txt = []
				self.boxLocks = []

				for i,x in enumerate(self.indexes):
					self.baseHlayouts.append(self.gener(i,x))

				for l in self.baseHlayouts:
					tmp = QWidget()
					tmp.setLayout(l)
					self.baseHlayout.addWidget(tmp)

			else:
				txt = self.sender()
				txt.setText(str(d+1))
		except:
			txt = self.sender()
			txt.setText(str(self.indexes[g] + 1))
	#--------------------------------------------------------------------------------------------
	# Metoda pro pregenerovani skupiny fotografii pro vyber, pri presunu
	def gener(self,g,indexDupl):
		tmp = QWidget()

		if not (len(self.dupls)):
			lh = QHBoxLayout()
			lh.setContentsMargins(1, 1, 2, 1)

			if(loading.editMet):
				s="Žádné fotografie nenalezeny."
			else:
				s="Žádné duplikace nenalezeny."

			lab = QLabel(s, self)
			lab.setAlignment(Qt.AlignCenter)
			lab.setFont(f3)

			lh.addWidget(lab)

			return lh
		elif not(loading.editMet):
			txtActPos = QLineEdit(self)
			lockBox = QCheckBox(self)

			lockBox.setText("Zamknout")
			lockBox.setChecked(self.locks[g])
			lockBox.setFont(f)
			lockBox.stateChanged.connect(lambda: self.changeStateCheckBoxLock(g))

			lh = QHBoxLayout()
			lv = QVBoxLayout()
			lh.setContentsMargins(1, 1, 1, 1)
			lv.setContentsMargins(1, 1, 1, 1)
			lab = QLabel("Skupina ", self)
			lab.setAlignment(Qt.AlignVCenter)
			lab.setFont(f3)
			txtActPos.setText(str(indexDupl+1))
			txtActPos.setFixedSize(80, 35)

			txtActPos.setFont(f3)
			txtActPos.textChanged.connect(lambda: self.txtChanged(g))
			txtActPos.setAlignment(Qt.AlignCenter)

			self.txt.append(txtActPos)
			self.boxLocks.append(lockBox)

			lab2 = QLabel(" z " + str(len(self.dupls)), self)
			lab2.setAlignment(Qt.AlignVCenter)
			lab2.setFont(f3)

			lh.addStretch()
			lh.addWidget(lab)
			lh.addWidget(txtActPos)
			lh.addWidget(lab2)

			lh.addWidget(lockBox)
			lh.addStretch()

			tmp.setLayout(lh)

		layout = QVBoxLayout()
		layout.setContentsMargins(1, 1, 1, 1)

		scrollArea = QScrollArea(self)
		scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

		self.qb.append([])
		self.btnInf.append([])
		self.labelPics.append([])
		self.labels.append([])
		self.labelsPath.append([])
		self.labelsInf.append([])
		self.labelsPerc.append([])

		layout.addWidget(tmp)

		scroLayoutV=QVBoxLayout(self)
		scroLayoutV.setContentsMargins(1, 1, 0, 0)
		
		widget=QWidget(self)

		for index,item in enumerate(self.dupls[indexDupl][1]):
			scroLayoutH=QHBoxLayout(self)
			lV=QVBoxLayout(self)
			lVl=QVBoxLayout(self)
			lh=QHBoxLayout(self)
			lh2=QHBoxLayout(self)
			lh3=QHBoxLayout(self)
			lh4=QHBoxLayout(self)
			lh5=QHBoxLayout(self)
			lh6=QHBoxLayout(self)

			scroLayoutH.setContentsMargins(0,0,0,0)
			lV.setContentsMargins(0,0,0,0)
			lVl.setContentsMargins(0,0,0,0)
			lh.setContentsMargins(0,0,0,0)
			lh2.setContentsMargins(0,0,0,0)
			lh3.setContentsMargins(0,0,0,0)
			lh4.setContentsMargins(0,0,0,0)
			lh5.setContentsMargins(0, 0, 0, 0)
			lh6.setContentsMargins(0, 0, 0, 0)

			self.labels[g].append(QLabel(getName(item),self))
			self.labels[g][index].setStyleSheet("QLabel {font-size: 11pt;font-weight: bold;color: #000000;}")
			self.labels[g][index].setAlignment(Qt.AlignTop)
			self.labels[g][index].setAlignment(Qt.AlignLeft)
			self.labels[g][index].setMaximumHeight(25)

			self.labelsPath[g].append(QLabel(item.filename.replace("/", "\\"), self))
			self.labelsPath[g][index].setStyleSheet("QLabel {font-size: 10pt;color: #000000;}")
			self.labelsPath[g][index].setAlignment(Qt.AlignTop)
			self.labelsPath[g][index].setAlignment(Qt.AlignLeft)
			self.labelsPath[g][index].setMaximumHeight(20)

			info=getMetaData(item)
			infoT=""
			width, height = item.size

			#infoT += str((datetime.datetime.strptime(time.ctime(os.path.getmtime(item.filename)), "%a %b %d %H:%M:%S %Y")).strftime('%d.%m.%Y %H:%M:%S')) + ', '
			try:
				infoT += time.strftime("%d.%m.%Y %H:%M:%S",time.strptime(info['DateTimeOriginal'],"%Y:%m:%d %H:%M:%S")) + ', '
			except:
				pass

			infoT += '(' + str(width) + 'x' + str(height) + ')' + ', '
			infoT += convertSize(os.path.getsize(item.filename))

			self.labelsInf[g].append(QLabel(infoT, self))
			self.labelsInf[g][index].setStyleSheet("QLabel {font-size: 10pt;color: #000000;}")
			self.labelsInf[g][index].setAlignment(Qt.AlignTop)
			self.labelsInf[g][index].setAlignment(Qt.AlignLeft)

			self.qb[g].append(QCheckBox("",self))
			self.btnInf[g].append(QPushButton("",self))
			self.labelPics[g].append(QLabel("", self))

			self.labelPics[g][index].setGeometry(0, -20, 235, 200)
			self.labelPics[g][index].setAlignment(Qt.AlignCenter)

			self.labelPics[g][index].setFrameShape(QFrame.Panel)
			self.labelPics[g][index].setFrameShadow(QFrame.Sunken)
			self.labelPics[g][index].setLineWidth(3)

			pixmap = QPixmap(item.filename)
			pixmap = pixmap.scaledToHeight(200)
			self.labelPics[g][index].setPixmap(pixmap)
			self.labelPics[g][index].setMaximumWidth(236)

			lh.addStretch()
			lh.addWidget(self.qb[g][index])
			lh.addStretch()

			lV.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh)
			lV.addWidget(tmp)

			lh2.addStretch()
			lh2.addWidget(self.btnInf[g][index])
			lh2.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh2)
			lV.addWidget(tmp)

			lV.addStretch()

			lh4.addWidget(self.labelPics[g][index])

			tmp = QWidget()
			tmp.setLayout(lV)
			lh5.addWidget(tmp)

			lVl.addStretch()

			lh3.addWidget(self.labels[g][index])
			lh3.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh3)
			lVl.addWidget(tmp)

			lh6.addWidget(self.labelsPath[g][index])
			lh6.addStretch()

			tmp = QWidget()
			tmp.setLayout(lh6)
			lVl.addWidget(tmp)

			lVl.addWidget(self.labelsInf[g][index])

			if(sett.qMeasure):
				perc = loading.qM[indexDupl][index]
				#color = "#000000"

				if(perc>=90):
					color = "#028436"
				elif(perc>50):
					color = "#cc5800"
				else:
					color="#c10505"

				self.labelsPerc[g].append(QLabel(str(perc)+'%', self))
				self.labelsPerc[g][index].setStyleSheet("QLabel {font-size: 14pt;color: "+color+";}")
				self.labelsPerc[g][index].setAlignment(Qt.AlignTop)
				self.labelsPerc[g][index].setAlignment(Qt.AlignLeft)

				lVl.addWidget(self.labelsPerc[g][index])

			lVl.addStretch()

			tmp = QWidget()
			tmp.setLayout(lVl)
			lh5.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(lh5)
			lh4.addWidget(tmp)

			tmp = QWidget()
			tmp.setLayout(lh4)
			scroLayoutH.addWidget(tmp)



			scroLayoutH.addStretch()

			tmp = QWidget()
			tmp.setLayout(scroLayoutH)
			scroLayoutV.addWidget(tmp)

			self.btnInf[g][index].setIcon(self.style().standardIcon(QStyle.SP_FileDialogInfoView))

			#self.qb[index].setStyleSheet("QCheckBox {font-size: 16pt;font-weight: bold;color: #000000;}")

			self.qb[g][index].show()
			self.btnInf[g][index].show()
		
		for item in self.dupls[indexDupl][0]:
			self.qb[g][item].setChecked(1)
		
		for item in self.qb[g]:
			item.stateChanged.connect(lambda: self.changeStateCheckBox(g,indexDupl))
		
		for index,item in enumerate(self.btnInf[g]):
			item.clicked.connect(lambda: self.infBox(g,indexDupl))
		
		widget.setLayout(scroLayoutV)
		scrollArea.setWidget(widget)

		layout.addWidget(scrollArea)

		return layout
	#--------------------------------------------------------------------------------------------
	# Metoda pro zobrazeni okna s informacemi o fotografii
	def infBox(self,g,ind):
		clickedButton = self.sender()
		index=self.btnInf[g].index(clickedButton)

		d=[]
		d.append(self.dupls[ind][1][index])

		showInfBox(d,True)
	#--------------------------------------------------------------------------------------------
	def changeStateCheckBoxLock(self,g):
		qBox=self.sender()

		self.locks[g]=qBox.isChecked()
	#--------------------------------------------------------------------------------------------
	# Metoda pro zmenu stavu jednoho z checkboxu, zmena oznaceni fotografie
	def changeStateCheckBox(self,g,ind):
		pom=self.qb[g]

		self.dupls[ind][0]=[]

		for index,item in enumerate(pom):
			if(item.isChecked()):
				self.dupls[ind][0].append(index)
	#--------------------------------------------------------------------------------------------
	def initWidget(self, items):
		listBox = QVBoxLayout(self)
		self.setLayout(listBox)
		
		scroll = QScrollArea(self)
		listBox.addWidget(scroll)
		scroll.setWidgetResizable(True)
		scrollContent = QWidget(scroll)
		
		scrollLayout = QVBoxLayout(scrollContent)
		scrollContent.setLayout(scrollLayout)
		for item in items:
			scrollLayout.addWidget(item)
		scroll.setWidget(scrollContent)
	#--------------------------------------------------------------------------------------------
	# Metoda pro posun v poli skupin duplikaci
	def moveDup(self,prep,base,step):
		if(prep):
			ret = base+ step
		else:
			ret = base - step
		
		if(ret<0):
			ret=len(self.dupls)-1
		elif(ret>(len(self.dupls)-1)):
			ret=0

		return ret
	#--------------------------------------------------------------------------------------------
	def stats(self):
		resText = ""

		endTime = time.time()
		files = 0
		dupls = 0
		ignFiles = 0
		duplsGroups = 0

		for d in loading.retComPic[0][0]:
			files += 1

			if (len(d[1]) == 1):
				continue

			duplsGroups += 1

			for f in d[1]:
				dupls += 1

		for i in loading.retComPic[0][1]:
			ignFiles += 1

		dupls -= duplsGroups
		resTime = (endTime - loading.startTime) / 60
		files += dupls

		resText += "Počet všech fotografií: "
		resText += str(files + ignFiles)
		resText += "\n"

		resText += "Počet fotografií bez metadat: "
		resText += str(ignFiles)
		resText += "\n"

		resText += "Duplikace [%]: "
		if (files):
			resText += str(round((dupls / files) * 100, 2))
		else:
			resText += str(0.00)
		resText += "\n"

		resText += "Čas [min]: "
		resText += str(round(resTime, 4))
		resText += "\n"

		showNotBox(resText, "Statistiky vyhledání duplikací",1)
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		global loading

		for load in loading.retComPic:
			for pic in load[1]:
				pic.close()
			for l in load[0]:
				for pic in l[1]:
					pic.close()

		loading = Loading()

		if (len(windows.list)):
			windows.list[0].disableAllowElements(True)

		closeDuplWindows()

		windows.list[0].folders = []

		loading.sorted = True
		loading.sortedKey = ""

		loading.editMet = 0

		event.accept()
	#--------------------------------------------------------------------------------------------
	# Metoda pro provedeni exportu oznacenych fotografii do ZIP archivu
	def exportToZip(self):
		fileName = self.showSaveDialog()
		zipFile = None

		fileName = fileName[0].split('.')[0] + fileName[1]

		if (len(fileName)):
			try:
				zipFile = zipfile.ZipFile(fileName, 'w')
			except PermissionError:
				showNotBox("Chyba přístupu!", "Chyba",1)
				return

			for indDupl, dupl in enumerate(self.dupls):
				for index in dupl[0]:
					f = self.dupls[indDupl][1][index]

					zipFile.write(f.filename, getFolder(f) + "/" + basename(f.filename))

	#--------------------------------------------------------------------------------------------
	# Metoda pro zobrazeni okna pro vyber cile kam se ma archiv ulozit
	def showSaveDialog(self):
		fileName = QFileDialog.getSaveFileName(self, 'Uložit export', '/export', '.zip', '')

		if (fileName):
			return fileName
		else:
			return ''

	#--------------------------------------------------------------------------------------------
	# Metoda pro zobrazeni okna s ignorovanymi fotografiemi
	def ignor(self):
		showIgnoredBox(self.ignored)
#--------------------------------------------------------------------------------------------
#Trida pro zakladni okno aplikace, zadani zdrojovych slozek, spusteni vyhledani
class BaseBox(QWidget):
	signalLoadEnd = pyqtSignal()
	signalLoad = pyqtSignal()

	# --------------------------------------------------------------------------------------------
	# Metoda pro inicializaci komponenty
	def __init__(self):
		super().__init__()
		self.title = 'DupFot'
		self.setWindowIcon(QIcon('./resources/dupFot.ico'))
		self.indexDupl=0
		self.height=700
		self.width=900
		self.folders=[]
		self.tFolder='/home'
		self.textEdit=[]
		self.qCh=[]
		self.btnChooser=[]
		self.qChMaster = QCheckBox(self)
		self.retComPictr=[]
		self.buttons=[]

		self.folderLayout=QVBoxLayout(self)
		self.scrollArea = QScrollArea(self)

		self.threads=Threads()

		self.initUI()
	#--------------------------------------------------------------------------------------------
	# Metoda pro sestaveni komponenty
	def initUI(self):
		self.setWindowTitle(self.title)

		self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
		self.scrollArea.setAlignment(Qt.AlignHCenter)

		self.setGeometry(self.width, self.height, self.width, self.height)

		qtRectangle = self.frameGeometry()
		centerPoint = QDesktopWidget().availableGeometry().center()
		qtRectangle.moveCenter(centerPoint)
		self.move(qtRectangle.topLeft())

		lh=QHBoxLayout(self)
		lh2 = QHBoxLayout(self)
		lv = QVBoxLayout(self)

		lh.setContentsMargins(2, 4, 2, 0)
		lh2.setContentsMargins(2, 0, 2, 0)
		lv.setContentsMargins(2,0,2,0)


		self.qChMaster.stateChanged.connect(self.changeQcheckBoxs)
		self.qChMaster.setToolTip("Ovládá všechny zobrazené.")
		self.buttons.append(self.qChMaster)

		lh.addStretch()
		lh.addWidget(self.qChMaster)

		btnFilters = QPushButton("", self)
		btnFilters.setIcon(QIcon('./resources/filters-icon.png'))
		btnFilters.setIconSize(QSize(25,25))
		btnFilters.clicked.connect(self.setFilters)
		btnFilters.setFixedSize(40, 40)
		btnFilters.setStyleSheet("QPushButton {font-size: 19pt;font-weight: bold;color: #000000;}")
		btnFilters.setToolTip("Filtry")
		self.buttons.append(btnFilters)

		lh.addWidget(btnFilters)

		btnSettings = QPushButton("", self)
		btnSettings.setIcon(QIcon('./resources/settings-icon.png'))
		btnSettings.setIconSize(QSize(25, 25))
		btnSettings.clicked.connect(self.setSettings)
		btnSettings.setFixedSize(40, 40)
		btnSettings.setStyleSheet("QPushButton {font-size: 19pt;font-weight: bold;color: #000000;}")
		btnSettings.setToolTip("Nastavení")
		self.buttons.append(btnSettings)

		lh.addWidget(btnSettings)

		btnNewFolder = QPushButton("", self)
		btnNewFolder.setIcon(self.style().standardIcon(QStyle.SP_FileDialogNewFolder))
		btnNewFolder.setIconSize(QSize(25, 25))
		btnNewFolder.clicked.connect(lambda: self.newFolder())
		btnNewFolder.setFixedSize(40, 40)
		btnNewFolder.setToolTip("Přidat složku")
		self.buttons.append(btnNewFolder)

		lh.addWidget(btnNewFolder)

		lh.addStretch()

		btnHelp = QPushButton("", self)
		btnHelp.setIcon(self.style().standardIcon(QStyle.SP_MessageBoxQuestion))
		btnHelp.setIconSize(QSize(25, 25))
		btnHelp.clicked.connect(self.help)
		btnHelp.setFixedSize(40, 40)
		btnHelp.setToolTip("Nápověda")

		lh.addWidget(btnHelp)

		"""
		if (sett.fullScreen):
			btnClose = QPushButton("Zavřít", self)
			btnClose.clicked.connect(self.close)
			btnClose.setFixedSize(80, 40)
			btnClose.setFont(f3)

			lh.addWidget(btnClose)
		"""

		tmp = QWidget()
		tmp.setLayout(lh)
		lv.addWidget(tmp)


		widget = QWidget()
		widget.setLayout(self.folderLayout)
		self.scrollArea.setWidget(widget)
		lv.addWidget(self.scrollArea)

		btnAccept = QPushButton("Najít duplikace", self)
		btnAccept.clicked.connect(self.findDupls)
		btnAccept.setMinimumSize(200,50)
		btnAccept.setFont(f3)
		self.buttons.append(btnAccept)

		lh2.addStretch()
		lh2.addWidget(btnAccept)

		btnEditMet = QPushButton("Editovat metadata", self)
		btnEditMet.clicked.connect(self.editMetadata)
		btnEditMet.setMinimumSize(220, 50)
		btnEditMet.setFont(f3)
		self.buttons.append(btnEditMet)

		lh2.addWidget(btnEditMet)
		lh2.addStretch()

		tmp = QWidget()
		tmp.setLayout(lh2)
		lv.addWidget(tmp)

		self.newFolder(t="Zde zadejte složku s fotografiemi.")

		for i in range(0,14):
			self.newFolder()

		self.setLayout(lv)

		self.show()
	#--------------------------------------------------------------------------------------------
	def help(self):
		text  = "Dokumentace: https://www.overleaf.com/read/ktvjkhhqzqhm\n"
		text += "Repozitář: https://gitlab.com/Sklenar/dupFot.git\n"
		text += "\n"
		text += "Metadata jsou textová data, která popisují fotografii.\n Jsou vytvořena v okamžiku vzniku fotografie snímacím zařízením.\n\n"
		text += "Ve skupině duplikací mají všechny fotografie stejná metadata.\n"
		text += "Fotografie bez metadat jsou ignorovány.\n"
		text += "\n"
		text += "Při zobrazení detailního náhledu fotografií/e lze přibližovat obraz následujícími způsoby:\n"
		text += 	"podržením pravého tlačítka myši označte oblast pro přiblížení\n"
		text +=		"podržením levého tlačítka myši posunujte oblast\n"
		text += 	"kolečko myši slouží pro přiblížení v oblasti\n"
		text += 	"dvojité kliknutí pravého tlačítka myši vrátí počáteční stav.\n"
		
		showNotBox(text,"Nápověda",1)
	#--------------------------------------------------------------------------------------------
	# Metoda ktera se vykona automaticky pri ukonceni komponenty
	def closeEvent(self, event):
		for w in windows.list:
			w.close()
		
		windows.list.remove(self)
		
		if(len(self.threads.list)):
			joinThread(0, self.threads)
		else:
			for i in range(0, len(self.threads.list)-1):
				joinThread(i, self.threads)

		event.accept()
	#--------------------------------------------------------------------------------------------
	def changeQcheckBoxs(self):
		for i in self.qCh:
			i.setChecked(self.qChMaster.isChecked())
	#--------------------------------------------------------------------------------------------
	def setFilters(self):
		showFiltersBox()
	# --------------------------------------------------------------------------------------------
	def setSettings(self):
		showSettingsBox()
	#--------------------------------------------------------------------------------------------
	def newFolder(self,t=""):
		lh=QHBoxLayout(self)
		lh.setContentsMargins(0, 0, 0, 0)

		labelSubFolders = QLabel("Podsložky:", self)
		labelSubFolders.setFont(f2)

		qCh = QCheckBox("", self)
		qCh.setStyleSheet("QCheckBox {font-size: 18pt;font-weight: bold;color: #000000;}")
		qCh.setChecked(sett.subFolders)

		textFolderChooser = QLineEdit(t, self)
		textFolderChooser.setFixedHeight(24)
		textFolderChooser.setMinimumWidth(700)
		textFolderChooser.setStyleSheet("QLineEdit {font-size: 9pt;font-weight: bold;color: #000000;}")

		btnFolderChooser = QPushButton("...", self)
		btnFolderChooser.clicked.connect(lambda: self.showFolderDialog(btnFolderChooser))
		btnFolderChooser.setFixedSize(20, 25)
		btnFolderChooser.setStyleSheet("QPushButton {font-size: 10pt;font-weight: bold;color: #000000;}")

		lh.addWidget(labelSubFolders)
		lh.addWidget(qCh)
		lh.addWidget(textFolderChooser)
		lh.addWidget(btnFolderChooser)

		tmp = QWidget()
		tmp.setLayout(lh)
		self.folderLayout.addWidget(tmp)

		widget = QWidget()
		widget.setLayout(self.folderLayout)
		self.scrollArea.setWidget(widget)

		self.btnChooser.append(btnFolderChooser)
		self.qCh.append(qCh)
		self.textEdit.append(textFolderChooser)

	#--------------------------------------------------------------------------------------------
	def showFolderDialog(self,btn):
		index=self.btnChooser.index(btn)

		if(self.tFolder.count("/")>1):
			fname = QFileDialog.getExistingDirectory(self, 'Open folder', self.tFolder.rsplit("/", 1)[0])
		else:
			fname = QFileDialog.getExistingDirectory(self, 'Open folder', self.tFolder)

		if(len(fname)):
			self.tFolder=fname
			self.textEdit[index].setText(self.tFolder)
	#--------------------------------------------------------------------------------------------
	def initWidget(self, items):
		listBox = QVBoxLayout(self)
		self.setLayout(listBox)
		
		scroll = QScrollArea(self)
		listBox.addWidget(scroll)
		scroll.setWidgetResizable(True)
		scrollContent = QWidget(scroll)
		
		scrollLayout = QVBoxLayout(scrollContent)
		scrollContent.setLayout(scrollLayout)
		for item in items:
			scrollLayout.addWidget(item)
		scroll.setWidget(scrollContent)
	#--------------------------------------------------------------------------------------------
	def updateFolders(self):
		#self.folders.append(["../data", True])
		#self.folders.append(["D:/test", True])
		#self.folders.append(["D:/test/1", True])

		for i, text in enumerate(self.textEdit):
			if (not (text.text() == '')):
				self.folders.append([text.text(), self.qCh[i].isChecked()])

		self.disableAllowElements(False)
	#--------------------------------------------------------------------------------------------
	def editMetadata(self):
		self.updateFolders()

		pictures=getPicturesFromFolders(self.folders)

		sk=[]
		sks=[]
		skss=[]

		p=pictures[1]

		# filtrovani
		p = filterByDate(p, filters.dateList)

		s = p + pictures[0]

		sk.append([])
		sk.append(s)

		sks.append(sk)
		skss.append(sks)
		skss.append([])

		loading.retComPic.append(skss)

		loading.editMet=1

		loading.sigLoad = self.signalLoad

		self.signalEmit()
	#--------------------------------------------------------------------------------------------
	def findDupls(self):
		self.updateFolders()

		lb=showLoadingBox()
		
		t=Thread(target=self.comPictures,args=(sett.preferenceMeasure,))
		self.threads.list.append(t)
		
		self.signalLoadEnd.connect(self.signalEmit)
		self.signalLoad.connect(lb.signalEmit)

		loading.sigLoad=self.signalLoad
		
		t.start()
	#--------------------------------------------------------------------------------------------
	def setFolders(self,f):
		self.folders=f

	#--------------------------------------------------------------------------------------------
	def comPictures(self, prepQ):
		ret = []
		threadsCom=Threads()

		files = getPicturesFromFolders(self.folders)

		ign = files[0]
		source = files[1]
		sourceFN=[]

		# filtrovani
		source = filterByDate(source, filters.dateList)

		loading.done = 0
		loading.max = 1

		for p in source:
			sourceFN.append(p.filename)

		self.signalLoad.emit()

		for index, item in enumerate(source[:len(source)]):
			# TODO rozdelit vlakna asi v polovine a zkusit kazdou polovinu pustit jinym procesem, pak by to mozna konecne mohlo pouzit dalsi jadro
			threadsCom.list.append(StoppableThread(name=index, target=dupl, args=(source, sourceFN, index, item, threadsCom,)))

		#runThreadsOnMultipleCores(threadsCom)

		for t in threadsCom.list:
			t.start()

		for t in threadsCom.list:
			t.join()

		if not(loading.stop):
			loading.done=loading.max-int(loading.max * 0.10)
			self.signalLoad.emit()

			if(sett.qMeasure):
				loading.setText("Vyhodnocuji kvalitu...")

			# spojeni vsech vlaken
			while not (len(threadsCom.list) == 0):
				if not (threadsCom.list[0].isStopped()):
					dp = threadsCom.list[0].res

					if (len(dp) > 1):
						ret.append([solvDup(dp, prepQ), dp])
					elif not (sett.onlyDupl):
						ret.append([[0], dp])

				threadsCom.list.pop(0)

			#serazeni skupin
			if(sett.sortGrups):
				#podle poctu fotografii ve skupine
				ret = sorted(ret, key=lambda s: len(s[1]), reverse=True)
			else:
				#podle casu, puvodni poradi
				ret = ret

			loading.retComPic.append([ret, ign])

			if(sett.qMeasure and not(len(loading.qM))):
				loading.qM=calcQualityMeasure(ret)

			if not(loading.stop):
				loading.done += int(loading.max * 0.10)
				loading.sigLoad.emit()

				self.signalLoadEnd.emit()
	#--------------------------------------------------------------------------------------------
	def signalEmit(self):
		if (len(self.threads.list)):
			joinThread(0, self.threads)
		else:
			for i in range(0, len(self.threads.list) - 1):
				joinThread(i, self.threads)

		showDuplsBox(loading.retComPic[0])
	#--------------------------------------------------------------------------------------------
	def disableAllowElements(self,prep):
		for btn in self.btnChooser:
			btn.setEnabled(prep)
		
		for chck in self.qCh:
			chck.setEnabled(prep)
		
		for text in self.textEdit:
			text.setEnabled(prep)
		
		for btn in self.buttons:
			btn.setEnabled(prep)
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni hlavniho okna
def showBaseBox():
	ex=BaseBox()
	windows.list.append(ex)
	app.exec()
	
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna se seznamem ignorovanych fotografii
def showIgnoredBox(ignored):
	ex = IgnoredPicturesBox(ignored)
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna se skupinami duplikaci
def showDuplsBox(d):
	for w in windows.list:
		if isinstance(w, DuplsBox):
			return
	
	for i,e in enumerate(windows.list):
		if isinstance(e, LoadingBox):
			windows.list.pop(i)

	loading.setText=None

	ex=DuplsBox(d[0],d[1])
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni potvrzovaciho okna
def showYesNoDupBox(dupWindow,dupls,prep):
	ex=YesNoDupBox(dupWindow,dupls,prep)
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni oznamovaciho okna
def showNotBox(text,tit,prep):
	ex=NotificationBox(text,tit,prep)
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna s nastavenim aplikace
def showSettingsBox():
	ex=SettingsBox()
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna pro nastaveni filtru
def showFiltersBox():
	ex=FiltersBox()
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna s informacemi o fotografii
def showInfBox(ims,prep):
	ex=InfBox(ims,prep)
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zobrazeni okna s postupem pri nacitani fotografii
def showLoadingBox():
	ex=LoadingBox()
	windows.list.append(ex)
	return ex
#--------------------------------------------------------------------------------------------
#Funkce pro zavreni vsech oken
def closeDuplWindows():
	#bez  or (isinstance(w,NotificationBox)) a (isinstance(w,YesNoDupBox))
	
	for w in windows.list:
		if ((isinstance(w,DuplsBox)) or (isinstance(w,IgnoredPicturesBox)) or
				(isinstance(w,InfBox))):
			
			windows.list.remove(w)
			w.close()
#--------------------------------------------------------------------------------------------
