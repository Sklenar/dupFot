from PyQt5.QtWidgets import QApplication
import sys
import pickle


from classes import *

#Soubor pro vytvoreni globalnich promennych
#(c)xsklen09 (Artonix14), FIT VUT Brno, 2018
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#Funkce pro ulozeni nastaveni ze souboru
def saveSettings(sett):
	with open('dupFot.sett', 'wb') as outfile:
		pickle.dump(sett, outfile, pickle.HIGHEST_PROTOCOL)
#--------------------------------------------------------------------------------------------
#Funkce pro nacteni nastaveni ze souboru
def loadSett():
	with open('dupFot.sett', 'rb') as inputfile:
		s=pickle.load(inputfile)

	return s
#--------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------
#Vytvoreni zakladnich globalnich promennych pro celou aplikaci

app = QApplication(sys.argv)
loading=Loading()
windows=Windows()
filters=Filters()

#nacteni nastaveni
try:
	sett=loadSett()
except FileNotFoundError:
	sett = Settings(True, 0, True, False, False, False, True,5,True,True,False,[False,False,False],False,2,True,0,False,1)